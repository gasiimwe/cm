> # MTWA ACCOUNT-SERVICE


## Initial Set up requires
* Generate RSA Key pairs and certificate 2048 or higher 
* Working instance of aws storage or min-io
* Notification Service for email notifications 

## ACCOUNTS

**REGISTRATION**
  Endpoint     : `/api/v1/account-service/accounts/clients`
  Protocol     : `POST`
  Content Type : `multipart`
  Response     : ``
  Code         : 204
  Form Fields  : 
  ```
  `first_name`            : Required Parameter 
  `last_name`             : Required Parameter
  `gender`                : Required Parameter [MALE , FEMALE]
  `email_address`         : Required Parameter 
  `phone_number`          : Required Parameter
  `id_type`               : Required Parameter
  `id_number`             : Required Parameter
  `password`              : Required Parameter
  `photo`                 : Optional Parameter [file field for image]
  ```
**ACCOUNT ACTIVATION (OTP ACTIVATION)**
  Endpoint     : `/api/v1/account-service/accounts/clients/activate`
  Protocol     : `PUT`
  Content Type : `application/json`
  Body         : `{"reference":"", "value":""}`
  Response Code: 204
  **NB**: The reference and the otp value will be delivered by email
	
**UPDATE USER DETAILS**
  Endpoint     : `/api/v1/account-service/accounts/clients`
  Protocol     : `PUT`
  Content Type : `multipart`
  Authorized   : `Bearer {Token}`
  Response     : ``
  Code         : 204
  Form Fields  : 
  ```
  `first_name`            : Required Parameter 
  `last_name`             : Required Parameter
  `gender`                : Required Parameter [MALE , FEMALE]
  `email_address`         : Required Parameter 
  `phone_number`          : Required Parameter
  `id_type`               : Required Parameter
  `id_number`             : Required Parameter
  `password`              : Required Parameter
  `photo`                 : Optional Parameter [file field for image]
  ```  

**DELETE**
  Endpoint     : `/api/v1/account-service/accounts/clients`
  Protocol     : `DELETE`
  Authorized   : `Bearer {Token}`        
  Code         : 204

**Request Password Recovery**
Endpoint     : `/api/v1/account-service/accounts/clients/recovery-password`
Protocol     : `PUT`        
Code         : 204
Payload      : `{"channel": [EMAIL|SMS], "channel_value": "x@mail.com|+256******"}` 

**Reset Password**
Endpoint     : `/api/v1/account-service/accounts/clients/reset-password`
Protocol     : `PUT`  
Code         : 204
Payload      : `{"reference":"", "password":""}`


**Fetch Image**
Endpoint     : `/api/v1/account-service/accounts/clients/images/{image-identifier}`
Protocol     : `GET`
Code         : 200
Payload      : `Binary WithContent type headers`


## ACM (Access Control Management)
**PENDING DEVELOPMENT**

**Add Permissions**
Endpoint     : `/api/v1/account-service/acm/permissions`
Protocol     : `POST`
Code         : 204
Payload      : `{"permissions": []}`
Authorization: `Role [SUPER_ADMINISTRATOR]` 

**Search For Permissions**
Endpoint     : `/api/v1/account-service/acm/permissions/_search`
Query Params : `q: string` `limit: int` `offset: int`
Protocol     : `GET`
Code         : 200
Authorization: `Role [SUPER_ADMINISTRATOR]`
Response     : {"data": [{"name": "permission-name"}], "total": `long value`}

**Remove Permissions**
Endpoint     : `/api/v1/account-service/acm/permissions`
Protocol	 : `DELETE`
Code         : 204
Payload      : `{"permissions": []}` 
Authorization: `Role [SUPER_ADMINISTRATOR]`

**Add Role**
Endpoint     : `/api/v1/account-service/acm/role`
Protocol     : `POST`
Code         : 204
Payload      : {"name": "role-name", "permissions": [`string list if permission names`]}
Authorization: `Role [SUPER_ADMINISTRATOR]`

**Search For Roles**
Endpoint     : `/api/v1/account-service/acm/role/_search`
Protocol     : `GET`
Query Params : `q: Query(string), limit:int, offset:int`
Code         : 200
Payload      : {"data": [{"name": "role-name", "permissions": [{"name": "permission name"}]}], "total": `long value`}
Authorization: `Role [SUPER_ADMINISTRATOR]`

**Update Role Permissions**
Endpoint     : `/api/v1/account-service/acm/role/{role-name}`
Protocol     : `PUT`
Code         :  204
Payload      : {"added_permissions": [`string list of permissions`], "removed_permissions": [`string list of permissions`]}

**Remove Permission**
Endpoint     : `/api/v1/account-service/acm/role/{role-name}`
Protocol     : `DELETE`
Code         :  204

## STAFF

**Register Staff**
Endpoint     : `api/v1/account-service/staff/register`
Protocol     : `POST`
Code         : 201
Authorization: `Role [SUPER_ADMINISTRATOR]`
Form Fields  :

 ```
  `first_name`            : Required Parameter 
  `middle_name`           : Optional Parameter
  `last_name`             : Required Parameter
  `gender`                : Optional Parameter [MALE , FEMALE]
  `email_address`         : Required Parameter 
  `phone_number`          : Required Parameter
  `id_type`               : Optional Parameter
  `id_number`             : Optional Parameter
  `username`              : Required Parameter
  `organization`          : Required Parameter
  `about`                 : Optional Parameter
  `photo`                 : Optional Parameter [multipart file field]
  `roles`                 : Required Parameter [`EXISTING ROLES FOR STAFF`] 
  ```

**Update Existing Staff**
Endpoint     : `api/v1/account-service/staff/{staff-identifier}`
Protocol     : `PUT`
Code         : 204
Authorization: `Role [SUPER_ADMINISTRATOR]`
Form Fields  :

 ```
  `first_name`            : Required Parameter 
  `middle_name`           : Optional Parameter
  `last_name`             : Required Parameter
  `gender`                : Optional Parameter [MALE , FEMALE]
  `email_address`         : Required Parameter 
  `phone_number`          : Required Parameter
  `id_type`               : Optional Parameter
  `id_number`             : Optional Parameter
  `username`              : Required Parameter
  `organization`          : Required Parameter
  `about`                 : Optional Parameter
  `roles`                 : Optional Parameter [Return what you want kept and added, exlude what you want removed]
  `photo`                 : Required Parameter [multipart file field]
 ```

**Search For Staff**
Endpoint     : `api/v1/account-service/staff`
Query Params : `q: Query(string), limit:int, offset:int`
Code         : 200
Protocol     : `GET`
Authorization: `Role [SUPER_ADMINISTRATOR]`
Response     : {"data": [{`user details without verification document identifier`}], "total": `int value for total`}
**

**Remove Staff Member**
Endpoint     : `api/v1/account-service/staff/{staff-identifier}`
Code         : 204
Protocol     : `DELETE`
Authorization: `Role [SUPER_ADMINISTRATOR]`
**

**Fetch Roles**
Endpoint     : `api/v1/account-service/staff/roles`
Protocol     : `GET`
Authorization: `Role [SUPER_ADMINISTRATOR]`
Response     : {"data": [{"name": "role-name", "permissions": [{"name": "permission-name"}]}], "total": `int value for total`}

## USER ENDPOINTS
**Fetch User By identifier**
Endpoint     : `/api/v1/account-service/acm/users/{identifier}`
Protocol     : `GET`
Authorization: `Role [*]`
Response     : User{**}

