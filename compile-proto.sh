#!/usr/bin/env bash

protoc -I protos/ protos/payloads.proto --go_out=app/domain/accounts/messenger/email
protoc -I protos/ protos/details.proto --go_out=app/domain/acm/users/details