package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/gasiimwe/cm/app"
	"gitlab.com/gasiimwe/cm/app/domain/operations"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
)

func main() {
	mode := flag.String("mode", "run", " for the app run mode")
	flag.Parse()

	if *mode == "run" || *mode == "RUN" {

		application := app.Application{}
		application.Start()
		c := make(chan os.Signal, 2)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		<-c
		application.Stop()
	} else if *mode == "migrate" || *mode == "MIGRATE" {
		if err := database.NewMigrationHandler().Migrate(); err != nil {
			logging.GetInstance().Error(err)
		}
	} else if *mode == "reset" || *mode == "RESET" {
		if err := database.NewMigrationHandler().Reset(); err != nil {
			logging.GetInstance().Error(err)
		}
	} else if *mode == "setup" || *mode == "SETUP" {
		if err := operations.NewServiceOperations().Setup(); err != nil {
			logging.GetInstance().Error(err)
		}
	} else if *mode == "administration" || *mode == "ADMINISTRATION" {
		if err := operations.NewServiceOperations().Administration(); err != nil {
			logging.GetInstance().Error(err)
		}
	}
}
