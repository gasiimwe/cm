DROP TRIGGER IF EXISTS on_session_removed ON session_tb;
DROP FUNCTION IF EXISTS log_time_online;
DROP TABLE session_log_tb;
DROP TABLE session_tb;

