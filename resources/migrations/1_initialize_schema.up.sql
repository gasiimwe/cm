CREATE TABLE IF NOT EXISTS permission_tb (
	id SERIAL PRIMARY KEY,
	name VARCHAR NOT NULL UNIQUE,
	idx BIGSERIAL NOT NULL UNIQUE
);

CREATE INDEX permission_index_idx ON permission_tb USING btree(idx);

CREATE TABLE IF NOT EXISTS role_tb (
	id SERIAL PRIMARY KEY,
	name VARCHAR NOT NULL UNIQUE,
	idx BIGSERIAL NOT NULL UNIQUE
);

CREATE INDEX role_index_idx ON role_tb USING btree(idx);

CREATE TABLE IF NOT EXISTS role_permissions (
	role_id BIGINT NOT NULL,
	permission_id BIGINT NOT NULL,
	CONSTRAINT role_permissions_pkey PRIMARY KEY (role_id, permission_id)
);

CREATE TABLE IF NOT EXISTS user_tb (
	id SERIAL PRIMARY KEY,
	first_name VARCHAR NOT NULL,
	last_name VARCHAR NOT NULL,
	gender VARCHAR NOT NULL,
	id_type VARCHAR NULL,
	id_number VARCHAR NULL,
	email_address VARCHAR NOT NULL UNIQUE,
	phone_number VARCHAR NOT NULL UNIQUE,
	username VARCHAR NOT NULL UNIQUE,
	password VARCHAR NOT NULL UNIQUE,
	uid UUID NOT NULL UNIQUE,
	audience VARCHAR NOT NULL,
	organization VARCHAR NULL,
	photo_reference VARCHAR NULL,
	activated BOOLEAN NOT NULL DEFAULT FALSE,
	idx BIGSERIAL NOT NULL UNIQUE,
	CONSTRAINT unq_number UNIQUE(id_number, id_type)
);

CREATE INDEX user_index_idx ON user_tb USING btree(idx);

CREATE TABLE IF NOT EXISTS user_roles (
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id)
);
