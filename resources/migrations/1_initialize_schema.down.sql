DROP INDEX permission_index_idx;
DROP INDEX role_index_idx;
DROP INDEX user_index_idx;
DROP TABLE user_roles;
DROP TABLE user_tb;
DROP TABLE role_permissions;
DROP TABLE role_tb;
DROP TABLE permission_tb;
