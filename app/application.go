package app

import (
	"fmt"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/gasiimwe/cm/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/cm/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/middleware"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
)

type Application struct {
	server           *server.ServerCore
	discoveryService discovery.Service
	configs          configuration.Configuration
	details          server.Details
	serviceResources *ServiceResources
	tracker          server.Tracker
}

func (a *Application) onMiddlewareRegistry(router *mux.Router) {
	logging.GetInstance().Info("MIDDLEWARE INITIALIZATION")
	router.Use(middleware.HttpMetricMiddleware(a.tracker))
	router.Use(middleware.LoggingMiddleware())
}

func (a *Application) onStartUp() bool {
	// services to start the system with
	logging.GetInstance().Info("START UP INITIALIZATION")
	a.serviceResources.Initialize()
	if configuration.IsConsulEnabled() {
		if err := a.discoveryService.Register(
			a.details.Name,
			a.configs.Port,
			[]string{a.details.Environment},
		); err != nil {
			logging.GetInstance().Error(err)
		} else {
			logging.GetInstance().Infof("REGISTERED: %s TO CONSUL", a.details.Name)
		}
	}
	return true
}

func (a *Application) onHealthCheck() (interface{}, error) {
	return a.details, nil
}

func (a *Application) onShutdown() bool {
	// services to shutdown and resources to clean up
	logging.GetInstance().Info("SHUTDOWN CALLED")
	if configuration.IsConsulEnabled() {
		if err := a.discoveryService.DeRegister(a.details.Name); err != nil {
			logging.GetInstance().Error(err)
		} else {
			logging.GetInstance().Infof("DE-REGISTERED: %s FROM CONSUL", a.details.Name)
		}
	}
	a.serviceResources.CleanUp()
	return true
}

func (a *Application) Start() {
	a.details = server.FetchApplicationDetails()
	if configuration.IsConsulEnabled() {
		a.initializeServiceDiscovery()
		a.configs = configuration.ParseFromConsul(a.discoveryService, a.details.Name)
	} else {
		a.configs = configuration.ParseFromResource()
	}
	privateKeyBytes, err := server.Asset("resources/private-rsa-key.pem")
	if err != nil {
		panic(err)
	}
	tokenVerifier := security.New(privateKeyBytes)
	a.server = server.New(
		a.details,
		a.configs.Host,
		a.configs.Port,
		tokenVerifier,
		a.configs.Connections,
	)
	a.serviceResources = ProduceServiceResources(
		a.discoveryService,
		a.server,
		a.configs,
	)
	a.tracker = server.NewTracker(a.server.GetMetricsRegistry())
	a.server.OnMiddlewareRegistration(a.onMiddlewareRegistry)
	a.server.OnStartUp(a.onStartUp)
	a.server.OnShutDown(a.onShutdown)
	a.server.OnHealthCheck(a.onHealthCheck)
	a.server.Start()
}

func (a *Application) initializeServiceDiscovery() {
	address := fmt.Sprintf("%s:%s", os.Getenv("CONSUL_HOST"), os.Getenv("CONSUL_PORT"))
	datacenter := os.Getenv("CONSUL_DATACENTER")
	token := os.Getenv("CONSUL_TOKEN")
	a.discoveryService = discovery.New(address, datacenter, token)
}

func (a *Application) Stop() {
	a.server.Stop()
}
