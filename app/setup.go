package app

import (
	"time"

	"gitlab.com/gasiimwe/cm/app/domain/accounts/clients"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/messenger"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/otp"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/recovery"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/staff"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/storage"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gitlab.com/gasiimwe/cm/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/rpc"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
)

type ServiceResources struct {
	discoveryService     discovery.Service
	server               *server.ServerCore
	permissionRepository permissions.PermissionRepository
	rolesRepository      roles.RoleRepository
	usersRepository      users.UserRepository
	recoveryRepository   recovery.RecoveryRepository
	otpRepository        otp.OTPRepository
	parameters           configuration.Configuration
	messengerClient      messenger.Client
	passwordHandler      security.PasswordHandler
	fileStorage          storage.FileStorage
	dataSource           database.DataSource
}

func ProduceServiceResources(
	discoveryService discovery.Service,
	server *server.ServerCore,
	parameters configuration.Configuration) *ServiceResources {
	return &ServiceResources{
		discoveryService: discoveryService,
		server:           server,
		parameters:       parameters,
	}
}

func (sr *ServiceResources) Initialize() {
	sr.dataSource = sr.createDataSource()
	sr.setUpRepositories()
	sr.setUpMessenger()
	sr.setUpPasswordHandler()
	sr.setUpFileStorage()
	webControllerRegistry := sr.server.GetWebControllerRegistry()
	natsControllerRegistry := sr.server.GetNATSControllerRegistry()
	sr.setUpACM(webControllerRegistry)
	sr.setUpStaffResource(webControllerRegistry)
	sr.setUpClientResource(webControllerRegistry)
	sr.setUpRPCResource(natsControllerRegistry)
}

func (sr *ServiceResources) createDataSource() database.DataSource {
	return database.
		NewBuilder().
		EnableLogging().
		MaxConnectionLifeTime(time.Hour).
		MetricRegistry(sr.server.GetMetricsRegistry()).
		MaxIdleConnections(sr.parameters.DatabaseParameters.MaxIdleConnections).
		MaxOpenConnections(sr.parameters.DatabaseParameters.MaxOpenConnections).
		Uri(sr.parameters.DatabaseParameters.URI).
		Build()
}

func (sr *ServiceResources) setUpFileStorage() {
	sr.fileStorage = storage.NewS3FileStorage(
		sr.parameters.StorageParameters.AccessKey,
		sr.parameters.StorageParameters.SecretKey,
		sr.parameters.StorageParameters.Host,
		sr.parameters.StorageParameters.Bucket,
		sr.parameters.StorageParameters.Region,
	)
}

func (sr *ServiceResources) setUpPasswordHandler() {
	sr.passwordHandler = &security.Argon2PasswordHandler{
		EncryptionParameters: sr.parameters.EncryptionParameters,
	}
}

func (sr *ServiceResources) setUpMessenger() {
	sr.messengerClient = messenger.New(sr.parameters.Connections, sr.parameters.Links)
	if err := sr.messengerClient.StartUp(); err != nil {
		logging.GetInstance().Error(err)
	}
}

func (sr *ServiceResources) setUpRepositories() {
	sr.permissionRepository = permissions.New(sr.dataSource)
	sr.rolesRepository = roles.New(sr.dataSource)
	sr.usersRepository = users.New(sr.dataSource)
	sr.recoveryRepository = recovery.New(sr.dataSource)
	sr.otpRepository = otp.New(sr.dataSource)

}

func (sr *ServiceResources) setUpACM(webControllerRegistry *web.HTTPControllerRegistry) {
	webControllerRegistry.AddController(permissions.NewController(sr.permissionRepository))
	webControllerRegistry.AddController(roles.NewController(sr.rolesRepository))
	webControllerRegistry.AddController(users.NewController(sr.usersRepository))
}

func (sr *ServiceResources) setUpStaffResource(webControllerRegistry *web.HTTPControllerRegistry) {
	service := staff.New(
		sr.usersRepository,
		sr.rolesRepository,
		sr.recoveryRepository,
		sr.passwordHandler,
		sr.messengerClient,
	)
	webControllerRegistry.AddController(staff.NewController(service, sr.fileStorage))
}

func (sr *ServiceResources) setUpClientResource(webControllerRegistry *web.HTTPControllerRegistry) {
	service := clients.New(
		sr.usersRepository,
		sr.recoveryRepository,
		sr.passwordHandler,
		otp.NewHandler(sr.otpRepository, sr.parameters.OTPDuration*time.Minute),
		sr.messengerClient,
	)
	webControllerRegistry.AddController(clients.NewController(service, sr.fileStorage))
}

func (sr *ServiceResources) setUpRPCResource(natControllerRegistry *rpc.NatControllerRegistry) {
	natControllerRegistry.Add(users.NewUserRPCController(sr.usersRepository))
}

func (sr ServiceResources) CleanUp() {
	sr.dataSource.Close()
	sr.messengerClient.Shutdown()
}
