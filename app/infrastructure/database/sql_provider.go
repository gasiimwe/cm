package database

import (
	"database/sql"
	"errors"
	"fmt"
	"net"
	"net/url"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

const (
	MAX_IDLE_CONNECTIONS     = 10
	MAX_OPEN_CONNECTIONS     = 100
	MAX_CONNECTION_LIFE_TIME = time.Hour
)

type parameters struct {
	Scheme   string
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

type SqlDataSource struct {
	db                    *gorm.DB
	_innerSqlReference    *sql.DB
	registry              *prometheus.Registry
	maxConnectionLifeTime time.Duration
	maxIdleConnections    int
	maxOpenConnections    int
	enableLogging         bool
}

func (ds *SqlDataSource) open(uri string) error {
	params, err := ds.uriParser(uri)
	if err != nil {
		return err
	}
	ds.db, err = ds.connectionResolver(&params)
	if err != nil {
		return err
	}
	ds._innerSqlReference, err = ds.db.DB()
	if err != nil {
		return err
	}
	ds._innerSqlReference.SetMaxIdleConns(ds.maxIdleConnections)
	ds._innerSqlReference.SetConnMaxLifetime(ds.maxConnectionLifeTime)
	ds._innerSqlReference.SetMaxOpenConns(ds.maxOpenConnections)
	ds.db.Use(New(params.Database, ds.registry))
	return nil
}

func (ds *SqlDataSource) Connection() *gorm.DB {
	return ds.db
}

func (ds *SqlDataSource) Close() error {
	return ds._innerSqlReference.Close()
}

func (ds *SqlDataSource) connectionResolver(params *parameters) (*gorm.DB, error) {

	newLogger := logger.New(
		logging.GetInstance(),
		logger.Config{
			SlowThreshold: time.Second,   // Slow SQL threshold
			LogLevel:      logger.Silent, // Log level
			Colorful:      true,          // Disable color
		},
	)
	configuration := &gorm.Config{Logger: newLogger}
	switch params.Scheme {
	case "postgres":
		dsn := fmt.Sprintf(
			"host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
			params.Host,
			params.Port,
			params.Username,
			params.Database,
			params.Password)
		return gorm.Open(postgres.Open(dsn), configuration)
	default:
		return nil, errors.New("Unknown Database type")
	}
}

func (ds *SqlDataSource) uriParser(uri string) (parameters, error) {
	var host, port, path, password string
	var err error
	u, err := url.Parse(uri)
	if err != nil {
		return parameters{}, err
	}
	if host, port, err = net.SplitHostPort(u.Host); err == nil {

	} else {
		host = u.Host
	}
	if strings.Contains(u.Path, "/") {
		path = strings.Split(u.Path, "/")[1]
	} else {
		path = u.Path
	}
	if pwd, ok := u.User.Password(); ok {
		password = pwd
	}
	return parameters{
		Scheme:   u.Scheme,
		Username: u.User.Username(),
		Password: password,
		Host:     host,
		Database: path,
		Port:     port,
	}, nil
}

type Builder struct {
	uri                   string
	maxOpenConnections    int
	maxConnectionLifeTime time.Duration
	maxIdleConnections    int
	registry              *prometheus.Registry
	enableLogging         bool
}

func NewBuilder() *Builder {
	return &Builder{}
}

func (b *Builder) Build() *SqlDataSource {
	if b.maxOpenConnections == 0 {
		b.maxOpenConnections = MAX_OPEN_CONNECTIONS
	}
	if b.maxConnectionLifeTime == 0 {
		b.maxConnectionLifeTime = MAX_CONNECTION_LIFE_TIME
	}
	if b.maxIdleConnections == 0 {
		b.maxIdleConnections = MAX_IDLE_CONNECTIONS
	}
	if b.uri == "" {
		panic("No database uri has been specified")
	}
	if b.registry == nil {
		panic("Metrics Registry has not been specified")
	}
	instance := &SqlDataSource{
		registry:              b.registry,
		maxConnectionLifeTime: b.maxConnectionLifeTime,
		maxIdleConnections:    b.maxIdleConnections,
		maxOpenConnections:    b.maxOpenConnections,
		enableLogging:         b.enableLogging,
	}
	if err := instance.open(b.uri); err != nil {
		panic(err)
	}

	return instance
}

func (b *Builder) Uri(uri string) *Builder {
	b.uri = uri
	return b
}

func (b *Builder) MetricRegistry(registry *prometheus.Registry) *Builder {
	b.registry = registry
	return b
}

func (b *Builder) MaxConnectionLifeTime(MaxConnectionLifeTime time.Duration) *Builder {
	b.maxConnectionLifeTime = MaxConnectionLifeTime
	return b
}

func (b *Builder) MaxIdleConnections(MaxIdleConnections int) *Builder {
	b.maxIdleConnections = MaxIdleConnections
	return b
}

func (b *Builder) MaxOpenConnections(MaxOpenConnections int) *Builder {
	b.maxOpenConnections = MaxOpenConnections
	return b
}

func (b *Builder) EnableLogging() *Builder {
	b.enableLogging = true
	return b
}
