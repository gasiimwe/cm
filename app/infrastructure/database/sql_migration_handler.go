package database

import (
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source"
	"gitlab.com/gasiimwe/cm/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/cm/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
)

type SqlMigrationHandler struct {
	uri              string
	driver           source.Driver
	discoveryService discovery.Service
	configs          configuration.Configuration
	details          server.Details
}

func NewMigrationHandler() MigrationHandler {
	instance := &SqlMigrationHandler{}
	instance.initialize()
	return instance
}

func (smh *SqlMigrationHandler) initialize() {
	var err error
	smh.driver, err = WithInstance("resources/migrations")
	if err != nil {
		logging.GetInstance().Error(err)
	}
	smh.details = server.FetchApplicationDetails()
	if configuration.IsConsulEnabled() {
		smh.initializeServiceDiscovery()
		smh.configs = configuration.ParseFromConsul(smh.discoveryService, smh.details.Name)
	} else {
		smh.configs = configuration.ParseFromResource()
	}
	logging.GetInstance().Infof("URI:%s", smh.configs.DatabaseParameters.URI)
}
func (smh *SqlMigrationHandler) Reset() error {
	logging.GetInstance().Info("RESET - DATABASE")
	m, err := migrate.NewWithSourceInstance("embedded", smh.driver, smh.configs.DatabaseParameters.URI)
	if err != nil {
		return err
	}
	return m.Down()
}

func (smh *SqlMigrationHandler) Migrate() error {
	logging.GetInstance().Info("MIGRATE - DATABASE")
	m, err := migrate.NewWithSourceInstance("embedded", smh.driver, smh.configs.DatabaseParameters.URI)
	if err != nil {
		return err
	}
	return m.Up()
}

func (smh *SqlMigrationHandler) initializeServiceDiscovery() {
	address := fmt.Sprintf("%s:%s", os.Getenv("CONSUL_HOST"), os.Getenv("CONSUL_PORT"))
	datacenter := os.Getenv("CONSUL_DATACENTER")
	token := os.Getenv("CONSUL_TOKEN")
	smh.discoveryService = discovery.New(address, datacenter, token)
}
