package database

import (
	"context"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gorm.io/gorm"
)

const (
	REFRESH_INTERVAL = 15
)

type DataSourceMetricPlugin struct {
	db             *gorm.DB
	name           string
	metricRegistry *prometheus.Registry
	statistics     *DatabaseStatistics
	labels         map[string]string
	refreshOnce    sync.Once
}

func New(Name string, MetricRegistry *prometheus.Registry) *DataSourceMetricPlugin {
	instance := &DataSourceMetricPlugin{
		metricRegistry: MetricRegistry,
		labels:         make(map[string]string),
		name:           Name,
	}
	return instance
}

func (dsm *DataSourceMetricPlugin) Name() string {
	return "gorm:prometheus"
}

func (dsm *DataSourceMetricPlugin) Initialize(db *gorm.DB) error {
	dsm.db = db
	dsm.statistics = newStats(dsm.labels, dsm.metricRegistry)
	dsm.refreshOnce.Do(func() {
		go func() {
			for range time.Tick(time.Duration(REFRESH_INTERVAL) * time.Second) {
				dsm.refresh()
			}
		}()
	})
	return nil
}

func (dsm *DataSourceMetricPlugin) refresh() {
	if db, err := dsm.db.DB(); err == nil {
		dsm.statistics.Set(db.Stats())
	} else {
		dsm.db.Logger.Error(context.Background(), "gorm:prometheus failed to collect db status, got error: %v", err)
	}
}
