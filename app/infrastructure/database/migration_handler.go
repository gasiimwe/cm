package database

type MigrationHandler interface {
	Reset() error
	Migrate() error
}
