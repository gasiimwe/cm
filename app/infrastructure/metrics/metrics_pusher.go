package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type MetricsPusher struct {
	registry *prometheus.Registry
	handler  http.HandlerFunc
}

// New returns a new `Metric` instance. The `custom` helps keeping or discarding
// all the built-in default metrics. If you just want to see your custom metrics
// set it to `true`.
func New(custom bool) MetricsPusher {
	reg := prometheus.NewRegistry()

	if custom {
		return MetricsPusher{
			registry: reg,
			handler: func(w http.ResponseWriter, r *http.Request) {
				promhttp.HandlerFor(reg, promhttp.HandlerOpts{}).ServeHTTP(w, r)
			},
		}

	} else {
		return MetricsPusher{
			registry: reg,
			handler: func(w http.ResponseWriter, r *http.Request) {
				promhttp.Handler().ServeHTTP(w, r)
			},
		}
	}
}

// Handler returns HTTP handler.
func (mp MetricsPusher) Handler() http.HandlerFunc {
	return mp.handler
}

// Registry returns `Registry` instance which helps registering the custom metric collectors.
func (mp MetricsPusher) Registry() *prometheus.Registry {
	return mp.registry
}
