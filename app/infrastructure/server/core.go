package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	h "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	nats "github.com/nats-io/nats.go"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/metrics"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/rpc"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type ServerCore struct {
	tokenVerifier                  security.TokenVerifier
	routerRegistry                 registry.RouterRegistry
	topicRegistry                  registry.TopicRegistry
	router                         *mux.Router
	server                         *http.Server
	webControllerRegistry          *web.HTTPControllerRegistry
	natsControllerRegistry         *rpc.NatControllerRegistry
	host                           string
	port                           int
	startupCallback                func() bool
	shutdownCallback               func() bool
	healthCheckCallback            func() (interface{}, error)
	middlewareRegistrationCallback func(router *mux.Router)
	metricsPusher                  metrics.MetricsPusher
	details                        Details
	connection                     *nats.Conn
	servers                        []string
}

func New(details Details, host string, port int, tokenVerifier security.TokenVerifier, servers []string) *ServerCore {
	instance := &ServerCore{
		details:       details,
		host:          host,
		port:          port,
		tokenVerifier: tokenVerifier,
		servers:       servers,
	}
	instance.initialize()
	return instance
}

func (sc *ServerCore) initialize() {
	LOG := logging.GetInstance()
	var err error
	sc.connection, err = nats.Connect(strings.Join(sc.servers, ","),
		nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
			LOG.Errorf("Got disconnected! Reason: %q\n", err)
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			LOG.Infof("Got reconnected to %v!\n", nc.ConnectedUrl())
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			LOG.Errorf("Connection closed. Reason: %q\n", nc.LastError())
		}),
	)
	if err != nil {
		LOG.Error(err)
		panic(err)
	} else if sc.connection.IsConnected() {
		LOG.Infof("CONNECTED TO NATS.IO ")
	}

	sc.router = mux.NewRouter()
	sc.routerRegistry = registry.NewRouteRegistry(sc.router, sc.tokenVerifier)
	sc.topicRegistry = registry.NewTopicRegistry(sc.connection)
	sc.webControllerRegistry = web.New(sc.routerRegistry)
	sc.natsControllerRegistry = rpc.New(sc.topicRegistry)
	sc.metricsPusher = metrics.New(true)
}

func (sc *ServerCore) OnMiddlewareRegistration(middlewareRegistrationCallback func(router *mux.Router)) {
	sc.middlewareRegistrationCallback = middlewareRegistrationCallback
}

// GetWebControllerRegistry : Get an instance of the web controller registry
func (sc *ServerCore) GetWebControllerRegistry() *web.HTTPControllerRegistry {
	return sc.webControllerRegistry
}

// GetNATSControllerRegistry : Get an instance of the nats controller registry
func (sc *ServerCore) GetNATSControllerRegistry() *rpc.NatControllerRegistry {
	return sc.natsControllerRegistry
}

func (sc *ServerCore) GetMetricsRegistry() *prometheus.Registry {
	return sc.metricsPusher.Registry()
}

// OnStartUp : Triggered on a start up event
func (sc *ServerCore) OnStartUp(startupCallback func() bool) {
	sc.startupCallback = startupCallback
}

// OnShutDown : Triggered on a shutdown event
func (sc *ServerCore) OnShutDown(shutdownCallback func() bool) {
	sc.shutdownCallback = shutdownCallback
}

// OnHealthCheck : Triggered on a call to the HC endpoint
func (sc *ServerCore) OnHealthCheck(healthCheckCallback func() (interface{}, error)) {
	sc.healthCheckCallback = healthCheckCallback
}

// Start Server
func (sc *ServerCore) Start() {
	sc.webControllerRegistry.AddController(web.NewHealthChecker(sc.healthCheckCallback))
	sc.webControllerRegistry.AddController(web.NewMetricController(sc.metricsPusher))
	if sc.startupCallback() {
		sc.middlewareRegistrationCallback(sc.router)
		sc.webControllerRegistry.InitializeControllers()
		sc.natsControllerRegistry.InitializeControllers()
		sc.startUpServer()
	}
}

func (sc *ServerCore) startUpServer() {
	sc.server = &http.Server{
		Addr: fmt.Sprintf("%s:%d", sc.host, sc.port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler: h.CORS(h.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
			h.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS", "DELETE", "PATCH"}),
			h.AllowedOrigins([]string{"*", "localhost"}))(sc.router),
	}
	sc.routerRegistry.Initialize()
	sc.topicRegistry.Initialize()
	go func() {
		sc.logoPrint()
		LOG := logging.GetInstance()
		LOG.Infof("\n\n")
		LOG.Infof("Starting    %s \n", sc.details.Name)
		LOG.Infof("Version     %s \n", sc.details.Version)
		LOG.Infof("Environment %s \n", sc.details.Environment)
		LOG.Infof("Repository  %s \n", sc.details.Repository)
		LOG.Infof("Commit Hash %s \n", sc.details.Hash)
		LOG.Infof("Build Date  %s \n", sc.details.BuildDate)
		LOG.Infof("Build Epoch %d \n", sc.details.BuildEpoch)

		if err := sc.server.ListenAndServe(); err != nil {
			LOG.Error(err)
		}
	}()
}

func (sc *ServerCore) Stop() {
	if sc.shutdownCallback() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		sc.server.Shutdown(ctx)
		if sc.connection.IsConnected() {
			sc.connection.Close()
		}
		// Optionally, you could run srv.Shutdown in a goroutine and block on
		// <-ctx.Done() if your application should wait for other services
		// to finalize based on context cancellation.
		fmt.Println("shutting down")
		os.Exit(0)
	}
}

func (sc *ServerCore) logoPrint() {
	logoData, err := Asset("resources/boot.txt")
	if err != nil {
		logging.GetInstance().Error(err)
	} else {
		fmt.Print(string(logoData))
		fmt.Println()
	}
}
