package server

import (
	"net/http"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type MetricTracker struct {
	registry         *prometheus.Registry
	started          *prometheus.CounterVec
	latency          *prometheus.HistogramVec
	completed        *prometheus.CounterVec
	requestSize      *prometheus.HistogramVec
	responseSize     *prometheus.HistogramVec
	reporterInit     sync.Once
	reporterHistInit sync.Once
	reporterSizeInit sync.Once
}

func NewTracker(registry *prometheus.Registry) Tracker {

	trackerInstance := &MetricTracker{
		registry: registry,
	}
	trackerInstance.init()

	return trackerInstance
}

func (mt *MetricTracker) init() {
	mt.started = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_started_requests_total",
			Help: "Count of started requests.",
		},
		[]string{"host", "path", "method"},
	)
	mt.completed = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_completed_requests_total",
			Help: "Count of completed requests.",
		},
		[]string{"host", "path", "method", "status"},
	)
	mt.latency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_completed_latency_seconds",
			Help:    "Latency of completed requests.",
			Buckets: []float64{.01, .03, .1, .3, 1, 3, 10, 30, 100, 300},
		},
		[]string{"host", "path", "method", "status"},
	)
	mt.requestSize = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_size_bytes",
			Help:    "Size of sent requests.",
			Buckets: prometheus.ExponentialBuckets(32, 32, 6),
		},
		[]string{"host", "path", "method"},
	)
	mt.responseSize = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_response_size_bytes",
			Help:    "Size of received responses.",
			Buckets: prometheus.ExponentialBuckets(32, 32, 6),
		},
		[]string{"host", "path", "method", "status"},
	)
	mt.registry.MustRegister(mt.started)
	mt.registry.MustRegister(mt.completed)
	mt.registry.MustRegister(mt.latency)
	mt.registry.MustRegister(mt.requestSize)
	mt.registry.MustRegister(mt.responseSize)
}

func (mt *MetricTracker) RequestStarted(req *http.Request) {
	mt.started.WithLabelValues(req.URL.Host, req.URL.Path, req.Method).Inc()
}

func (mt *MetricTracker) RequestRead(req *http.Request, duration time.Duration, size int) {
	mt.requestSize.WithLabelValues(req.URL.Host, req.URL.Path, req.Method).Observe(float64(size))
}

func (mt *MetricTracker) ResponseStarted(req *http.Request, duration time.Duration, status int, header http.Header) {
	mt.completed.WithLabelValues(req.URL.Host, req.URL.Path, req.Method, string(status)).Inc()
	mt.latency.WithLabelValues(req.URL.Host, req.URL.Path, req.Method, string(status)).Observe(duration.Seconds())
}

func (mt *MetricTracker) ResponseDone(req *http.Request, duration time.Duration, status int, size int) {
	mt.responseSize.WithLabelValues(req.URL.Host, req.URL.Path, req.Method, string(status)).Observe(float64(size))
}
