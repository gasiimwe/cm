package web

import (
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type HTTPController interface {
	Name() string
	Initialize(RouteRegistry registry.RouterRegistry)
}
