package web

import (
	"gitlab.com/gasiimwe/cm/app/infrastructure/metrics"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type MetricController struct {
	*ControllerBase
	metricsPusher metrics.MetricsPusher
}

func NewMetricController(metricsPusher metrics.MetricsPusher) HTTPController {
	return &MetricController{metricsPusher: metricsPusher}
}

func (mc *MetricController) Name() string {
	return "metric-controller"
}

func (mc *MetricController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.Add(
		"/metrics",
		false,
		"GET",
		mc.metricsPusher.Handler(),
	)
}
