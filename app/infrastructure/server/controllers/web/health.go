package web

import (
	"net/http"

	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/health-check"

type HealthChecker struct {
	*ControllerBase
	healthCheckCallback func() (interface{}, error)
}

func NewHealthChecker(healthCheckCallback func() (interface{}, error)) HTTPController {
	return &HealthChecker{healthCheckCallback: healthCheckCallback}
}

func (hc *HealthChecker) Name() string {
	return "health-check-controller"
}

func (hc *HealthChecker) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.Add(
		BASE_PATH,
		false,
		"GET",
		hc.check,
	)
}

func (hc *HealthChecker) check(w http.ResponseWriter, r *http.Request) {
	data, err := hc.healthCheckCallback()
	if err != nil {
		logging.GetInstance().Error(err)
		hc.ServiceFailure(w, err)
	} else {
		hc.OK(w, data)
	}
}
