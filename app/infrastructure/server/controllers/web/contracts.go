package web

type Id struct {
	ID interface{} `json:"id"`
}

type Paged struct {
	Data  interface{} `json:"data"`
	Total int         `json:"total"`
}
