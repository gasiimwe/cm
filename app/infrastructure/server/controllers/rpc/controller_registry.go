package rpc

import (
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type NatControllerRegistry struct {
	topicRegistry registry.TopicRegistry
	controllers   map[string]NatController
}

func New(topicRegistry registry.TopicRegistry) *NatControllerRegistry {
	return &NatControllerRegistry{
		topicRegistry: topicRegistry,
		controllers:   make(map[string]NatController),
	}
}

func (cr *NatControllerRegistry) Add(controller NatController) {
	cr.controllers[controller.Name()] = controller
}

func (cr *NatControllerRegistry) InitializeControllers() {
	LOG := logging.GetInstance()
	for name, controller := range cr.controllers {
		LOG.Infof("CONTROLLER -> %s", name)
		controller.Initialize(cr.topicRegistry)
	}
}
