package rpc

import (
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type NatController interface {
	Name() string
	Initialize(TopicRegistry registry.TopicRegistry)
}
