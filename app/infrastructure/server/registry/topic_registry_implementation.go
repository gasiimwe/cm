package registry

import (
	nats "github.com/nats-io/nats.go"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
)

type Entry struct {
	Queue   string
	Topic   string
	Handler func(handler *nats.Msg)
}

func NewTopicRegistry(connection *nats.Conn) TopicRegistry {
	instance := &NatsIOTopicRegistry{connection: connection}
	instance.setup()
	return instance
}

type NatsIOTopicRegistry struct {
	entries    []*Entry
	connection *nats.Conn
}

func (nt *NatsIOTopicRegistry) setup() {
	nt.entries = make([]*Entry, 0)
}

func (nt *NatsIOTopicRegistry) Add(Topic string, Handler func(handler *nats.Msg)) {
	nt.entries = append(nt.entries, &Entry{Topic: Topic, Handler: Handler})
}

func (nt *NatsIOTopicRegistry) AddQueued(Queue, Topic string, Handler func(handler *nats.Msg)) {
	nt.entries = append(nt.entries, &Entry{Queue: Queue, Topic: Topic, Handler: Handler})
}

func (nt *NatsIOTopicRegistry) Initialize() {
	for _, h := range nt.entries {
		if len(h.Queue) != 0 {
			nt.connection.QueueSubscribe(h.Topic, h.Queue, h.Handler)
			logging.GetInstance().Infof("QUEUE %s TOPIC ->-> %s", h.Queue, h.Topic)
		} else {
			nt.connection.Subscribe(h.Topic, h.Handler)
			logging.GetInstance().Infof("TOPIC ->-> %s", h.Topic)
		}
	}
}
