package registry

import nats "github.com/nats-io/nats.go"

type TopicRegistry interface {
	Add(Topic string, Handler func(handler *nats.Msg))
	AddQueued(Queue, Topic string, Handler func(handler *nats.Msg))
	Initialize()
}
