package security

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	jwt "gopkg.in/square/go-jose.v2/jwt"
)

const (
	RS_256                 = "RS256"
	RS_384                 = "RS384"
	RS_512                 = "RS512"
	NO_SIGNING_KEY_PRESENT = "No Signing Key Present"
	MALFORMED_TOKEN_ERROR  = "Session Token is malformed"
	TOKEN_EXPIRED          = "Session has expired"
	INVALID_TOKEN_ERROR    = "Token is not valid or possibly not signed from this service"
)

var (
	ErrKeyMustBePEMEncoded = errors.New("Invalid Key: Key must be PEM encoded PKCS1 or PKCS8 private key")
	ErrNotRSAPrivateKey    = errors.New("Key is not a valid RSA private key")
	ErrNotRSAPublicKey     = errors.New("Key is not a valid RSA public key")
)

type RSATokenVerifier struct {
	privateKeyBytes []byte
	privateKey      *rsa.PrivateKey
	log             *logrus.Logger
}

func New(privateKeyBytes []byte) TokenVerifier {
	instance := &RSATokenVerifier{
		privateKeyBytes: privateKeyBytes,
		log:             logging.GetInstance(),
	}
	instance.init()
	return instance
}

func (ckv *RSATokenVerifier) init() {
	var err error
	ckv.privateKey, err = ParseRSAPrivateKeyFromPEM(ckv.privateKeyBytes)
	if err != nil {
		ckv.log.Fatal(err)
	}
	ckv.log.Println("Completed loading RSA Private Key")
}

func (ckv *RSATokenVerifier) VerifyToken(Token string) (*Claims, error) {
	claims := &Claims{}
	tok, err := jwt.ParseEncrypted(Token)
	if err != nil {
		ckv.log.Printf(err.Error())
		return claims, errors.New(MALFORMED_TOKEN_ERROR)
	}
	if err := tok.Claims(ckv.privateKey, &claims); err != nil {
		ckv.log.Printf(err.Error())
		return claims, errors.New(MALFORMED_TOKEN_ERROR)
	}
	if claims.Expiry.Time().Before(time.Now()) {
		return claims, errors.New(TOKEN_EXPIRED)
	}
	return claims, nil
}

// Parse PEM encoded PKCS1 or PKCS8 private key
func ParseRSAPrivateKeyFromPEM(key []byte) (*rsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKCS1PrivateKey(block.Bytes); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(block.Bytes); err != nil {
			return nil, err
		}
	}

	var pkey *rsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PrivateKey); !ok {
		return nil, ErrNotRSAPrivateKey
	}
	return pkey, nil
}

// Parse PEM encoded PKCS1 or PKCS8 private key protected with password
func ParseRSAPrivateKeyFromPEMWithPassword(key []byte, password string) (*rsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	var parsedKey interface{}

	var blockDecrypted []byte
	if blockDecrypted, err = x509.DecryptPEMBlock(block, []byte(password)); err != nil {
		return nil, err
	}

	if parsedKey, err = x509.ParsePKCS1PrivateKey(blockDecrypted); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(blockDecrypted); err != nil {
			return nil, err
		}
	}

	var pkey *rsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PrivateKey); !ok {
		return nil, ErrNotRSAPrivateKey
	}

	return pkey, nil
}
