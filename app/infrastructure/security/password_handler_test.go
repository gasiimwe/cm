package security_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
)

func TestPasswordHandler(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Password Handler Test Suite")
}

var _ = Describe("Password handler tests tests", func() {
	passwordHandler := security.Argon2PasswordHandler{
		EncryptionParameters: &security.EncryptionParameters{
			Memory:      64 * 1024,
			Iterations:  3,
			Parallelism: 2,
			SaltLength:  16,
			KeyLength:   32,
		},
	}
	var samplePassword = "pasword1234***"
	Context("Given a password is hashed", func() {

		It("The password will match the hash", func() {
			hash, _ := suite.PasswordHandler.CreateHash(samplePassword)
			isMatch, err := suite.PasswordHandler.CompareHashToPassword(samplePassword, hash)
			Expect(err).NotTo(HaveOccurred())
			Expect(isMatch).To(BeTrue())
		})

		It("A different password value will not be a match to the hash", func() {
			var nonMatchingPassword = "SomeFakePasswd"
			hash, _ := suite.PasswordHandler.CreateHash(SamplePassword)
			isMatch, _ := suite.PasswordHandler.CompareHashToPassword(nonMatchingPassword, hash)
			Expect(err).ToNot(HaveOccurred())
			Expect(isMatch).To(BeFalse())
		})

		It("An Empty password will not match the hash", func() {
			var emptyPassword = ""
			hash, _ := suite.PasswordHandler.CreateHash(samplePassword)
			_, err := suite.PasswordHandler.CompareHashToPassword(emptyPassword, hash)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(security.PASSWORD_IS_EMPTY))
		})
	})
	Context("Given an empty password", func() {
		var emptyPassword = ""
		It("No Hash will be created", func() {
			_, err := suite.PasswordHandler.CreateHash(emptyPassword)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(security.PASSWORD_IS_EMPTY))
		})

	})
	Context("Given an empty hash", func() {
		It("The password match will fail", func() {
			var emptyHash = ""
			_, err := suite.PasswordHandler.CompareHashToPassword(samplePassword, emptyHash)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(security.PASSWORD_HASH_IS_EMPTY))
		})
	})
})
