package discovery

import (
	"fmt"
	"net"

	consul "github.com/hashicorp/consul/api"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
)

type ConsulService struct {
	client     *consul.Client
	address    string
	datacenter string
	token      string
}

func New(address, datacenter string, token string) Service {
	instance := &ConsulService{
		address:    address,
		datacenter: datacenter,
		token:      token,
	}
	instance.init()
	return instance
}

func (cs *ConsulService) init() {
	var err error
	configuration := consul.DefaultConfig()
	configuration.Address = cs.address
	configuration.Datacenter = cs.datacenter
	configuration.Token = cs.token
	cs.client, err = consul.NewClient(configuration)
	if err != nil {
		logging.GetInstance().Error(err)
	}
}

func (cs *ConsulService) FetchConfigurationsData(ID string) ([]byte, error) {
	kv, _, err := cs.client.KV().Get(ID, &consul.QueryOptions{})
	if err != nil {
		return []byte{}, err
	}
	data := kv.Value
	return data, nil
}

func (cs *ConsulService) Service(service string, tag string) ([]string, error) {
	results := make([]string, 0)
	passingOnly := true
	entries, _, err := cs.client.Health().Service(service, tag, passingOnly, nil)
	if len(entries) == 0 && err == nil {

		return nil, fmt.Errorf("service ( %s ) was not found", service)
	}
	if err != nil {
		return nil, err
	}
	for _, entry := range entries {
		address := entry.Service.Address
		port := entry.Service.Port
		results = append(results, fmt.Sprintf("%s:%s", address, port))
	}
	return results, nil
}

// Register a service with local agent
func (cs *ConsulService) Register(name string, port int, tags []string) error {
	address := cs.getIP()
	health := fmt.Sprintf("http://%s:%d/health-check", address, port)
	registration := new(consul.AgentServiceRegistration)
	registration.ID = name
	registration.Name = name
	registration.Address = address
	registration.Port = port
	registration.Tags = tags
	registration.Check = new(consul.AgentServiceCheck)
	registration.Check.HTTP = health
	registration.Check.Interval = "5s"
	registration.Check.Timeout = "3s"
	return cs.client.Agent().ServiceRegister(registration)
}

// Deregister a service with local agent
func (cs *ConsulService) DeRegister(ID string) error {
	return cs.client.Agent().ServiceDeregister(ID)
}

func (cs *ConsulService) getIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}
