package configuration

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/gasiimwe/cm/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Host                 string                         `yaml:"host"`
	Port                 int                            `yaml:"port"`
	DatabaseParameters   *DatabaseParameters            `yaml:"database"`
	StorageParameters    *StorageParameters             `yaml:"aws"`
	EncryptionParameters *security.EncryptionParameters `yaml:"encryption"`
	OTPDuration          time.Duration                  `yaml:"otp-duration"`
	Connections          []string                       `yaml:"nats-servers"`
	Links                map[string]string              `yaml:"links"`
}

type StorageParameters struct {
	AccessKey string `yaml:"access-key"`
	SecretKey string `yaml:"secret-key"`
	Host      string `yaml:"url"`
	Bucket    string `yaml:"bucket"`
	Region    string `yaml:"region"`
}
type DatabaseParameters struct {
	URI                string `yaml:"uri"`
	MaxOpenConnections int    `yaml:"max-open-connections"`
	MaxIdleConnections int    `yaml:"max-idle-connections"`
}

func ParseFromResource() Configuration {
	yamlFile, err := server.Asset("resources/settings.yml")
	config := Configuration{}
	if err != nil {
		fmt.Println("Failed to open the file")
		panic(err)
	}
	yaml.Unmarshal(yamlFile, &config)
	return config
}

func ParseFromConsul(Service discovery.Service, Name string) Configuration {
	yamlFile, err := Service.FetchConfigurationsData(Name)
	config := Configuration{}
	if err != nil {
		fmt.Println("Failed to open the file")
		panic(err)
	}
	yaml.Unmarshal(yamlFile, &config)
	return config
}

func IsConsulEnabled() bool {
	value := os.Getenv("CONSUL_ENABLED")
	if len(value) == 0 || value != "true" {
		return false
	}
	return true
}
