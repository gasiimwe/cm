package middleware

import (
	"net/http"
	"time"

	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
)

func HttpMetricMiddleware(tracker server.Tracker) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		if tracker == nil {
			return next
		}
		return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
			start := time.Now()
			tracker.RequestStarted(req)
			req.Body = server.WrapBody(req.Body, func(size int) {
				tracker.RequestRead(req, time.Since(start), size)
			})
			wrapped := server.WrapWriter(resp, func(status int) {
				tracker.ResponseStarted(req, time.Since(start), status, resp.Header())
			})
			next.ServeHTTP(wrapped, req)
			tracker.ResponseDone(req, time.Since(start), wrapped.Status(), wrapped.Size())
		})
	}
}
