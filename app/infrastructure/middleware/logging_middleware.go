package middleware

import (
	"net/http"
	"runtime/debug"
	"time"

	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
)

// LoggingMiddleware logs the incoming HTTP request & its duration.
func LoggingMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {

					w.WriteHeader(http.StatusInternalServerError)
					logging.GetInstance().Error(err)
					logging.GetInstance().Error(debug.Stack())
				}
			}()

			start := time.Now()
			wrapped := wrapResponseWriter(w)
			next.ServeHTTP(wrapped, r)
			logging.GetInstance().Debugf(
				"status : %d method: %s path: %s duration: %d",
				wrapped.status,
				r.Method,
				r.URL.EscapedPath(),
				time.Since(start),
			)
		}

		return http.HandlerFunc(fn)
	}
}

type responseWriter struct {
	http.ResponseWriter
	status      int
	wroteHeader bool
}

func wrapResponseWriter(w http.ResponseWriter) *responseWriter {
	return &responseWriter{ResponseWriter: w}
}

func (rw *responseWriter) Status() int {
	return rw.status
}

func (rw *responseWriter) WriteHeader(code int) {
	if rw.wroteHeader {
		return
	}

	rw.status = code
	rw.ResponseWriter.WriteHeader(code)
	rw.wroteHeader = true

	return
}
