package roles

const (
	NO_PERMISSIONS_PRESENT = "No Permissions Were Found"
	NON_UNIQUE_ROLE        = "A Matching Role exists"
	ROLE_NOT_FOUND         = "No Role With This Name Exists"
)

// RoleRepository : Contract for Role Store operations
type RoleRepository interface {
	Add(Role *Role) error
	Update(Name string, AddedPermissions, RemovedPermissions []string) error
	Search(Query string, Limit, Offset int64) ([]*Role, error)
	Remove(Name string) error
	Fetch(Exclude []string) ([]*Role, error)
}
