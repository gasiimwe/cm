package roles_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestRolesRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Roles Repository Suite")
}

type testDataSource struct {
	db *gorm.DB
}

func (tds *testDataSource) Connection() *gorm.DB {
	if tds.db == nil {
		var err error
		tds.db, err = gorm.Open(
			sqlite.Open("file::memory:?mode=memory&cache=shared"),
			&gorm.Config{})
		if err != nil {
			panic(err)
		}
	}
	return tds.db
}

func (tds *testDataSource) Close() error {
	if tds.db != nil {
		return tds.Close()
	}
	return nil
}

func dataSourceProvider() database.DataSource {
	return &testDataSource{}
}

func migrate(dataSource database.DataSource) {
	rule0 := `CREATE TRIGGER IF NOT EXISTS update_permission_idx AFTER INSERT ON permission_tb
		   BEGIN
			UPDATE permission_tb SET idx=id WHERE id=NEW.id;
		   END;`

	rule1 := `CREATE TRIGGER update_role_idx AFTER INSERT ON role_tb
			  BEGIN
			   UPDATE role_tb SET idx=id WHERE id=NEW.id;
			  END;`
	dataSource.Connection().AutoMigrate(&permissions.Permission{}, &roles.Role{})
	dataSource.Connection().Exec(rule0)
	dataSource.Connection().Exec(rule1)
}

func addPermissions(dataSource database.DataSource, addedPermissions []string) {
	added := make([]permissions.Permission, 0)
	for _, permission := range addedPermissions {
		added = append(added, permissions.Permission{Name: permission})
	}
	permissions.New(dataSource).Add(added)
}

var _ = Describe("Permission store tests", func() {

	Context("Given a unique role is added with existing permissions", func() {
		dataSource := dataSourceProvider()
		migrate(dataSource)
		rolesRepository := roles.New(dataSource)
		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM role_tb")
			dataSource.Connection().Exec("DELETE FROM permission_tb")
		})
		BeforeEach(func() {
			addPermissions(dataSource, []string{"CAN_ACCESS_CART"})
			rolesRepository.Add(&roles.Role{
				Name:        "CUSTOMER",
				Permissions: []permissions.Permission{{Name: "CAN_ACCESS_CART"}}},
			)
		})

		It("Then the role can be retrieved", func() {
			results, err := rolesRepository.Search("CUSTOMER", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeEmpty())
			Expect(results[0].Name).To(Equal("CUSTOMER"))
			Expect(results[0].Permissions).NotTo(BeEmpty())
			Expect(results[0].Permissions[0].Name).To(Equal("CAN_ACCESS_CART"))
		})

		It("Then adding a role by the same name will fail", func() {
			err := rolesRepository.Add(&roles.Role{
				Name:        "CUSTOMER",
				Permissions: []permissions.Permission{{Name: "CAN_ACCESS_CART"}}},
			)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(roles.NON_UNIQUE_ROLE))
		})

		It("Then then roles can be searched for by partial name", func() {
			results, err := rolesRepository.Search("CUS", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeEmpty())
			Expect(results[0].Name).To(Equal("CUSTOMER"))
			Expect(results[0].Permissions).NotTo(BeEmpty())
			Expect(results[0].Permissions[0].Name).To(Equal("CAN_ACCESS_CART"))
		})

		It("Then the role can be removed", func() {
			err := rolesRepository.Remove("CUSTOMER")
			Expect(err).ToNot(HaveOccurred())
			results, err := rolesRepository.Search("CUSTOMER", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).To(BeEmpty())
		})
	})

	Context("Given the permissions do not exist", func() {
		dataSource := dataSourceProvider()
		migrate(dataSource)

		rolesRepository := roles.New(dataSource)
		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM role_tb")
			dataSource.Connection().Exec("DELETE FROM permission_tb")
		})
		BeforeEach(func() {
			addPermissions(dataSource, []string{"CAN_ACCESS_CART"})
			rolesRepository.Add(&roles.Role{
				Name:        "CUSTOMER",
				Permissions: []permissions.Permission{{Name: "CAN_ACCESS_CART"}}},
			)
		})
		It("Then a role cannot be created with permissions that don't exist", func() {
			err := rolesRepository.Add(&roles.Role{
				Name:        "THUG",
				Permissions: []permissions.Permission{{Name: "CAN_STEAL_CART"}}},
			)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(roles.NO_PERMISSIONS_PRESENT))
		})
	})

	Context("Given that a role already exists", func() {
		dataSource := dataSourceProvider()
		migrate(dataSource)
		rolesRepository := roles.New(dataSource)
		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM role_tb")
			dataSource.Connection().Exec("DELETE FROM permission_tb")
		})
		BeforeEach(func() {
			addPermissions(dataSource, []string{"CAN_FLIP_CART"})
			rolesRepository.Add(&roles.Role{
				Name:        "CUSTOMER",
				Permissions: []permissions.Permission{{Name: "CAN_FLIP_CART"}}},
			)
		})

		It("Then an existing permission can be added to a role", func() {
			addPermissions(dataSource, []string{"CAN_STEAL_CART"})
			err := rolesRepository.Update("CUSTOMER", []string{"CAN_STEAL_CART"}, []string{})
			Expect(err).ToNot(HaveOccurred())
			results, err := rolesRepository.Search("CUSTOMER", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(len(results[0].Permissions)).To(Equal(2))
		})

		It("Then a permission can be removed from a role", func() {
			addPermissions(dataSource, []string{"CAN_CLAIM_CART"})
			err := rolesRepository.Update("CUSTOMER", []string{"CAN_CLAIM_CART"}, []string{"CAN_FLIP_CART"})
			Expect(err).ToNot(HaveOccurred())
			results, err := rolesRepository.Search("CUSTOMER", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeEmpty())
			Expect(results[0].Permissions[0].Name).To(Equal("CAN_CLAIM_CART"))
		})
	})
})
