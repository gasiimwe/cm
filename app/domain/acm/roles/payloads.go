package roles

type RoleUpdate struct {
	AddedPermissions   []string `json:"added-permissions"`
	RemovedPermissions []string `json:"removed-permissions"`
}
