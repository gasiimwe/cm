package roles

import (
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
)

// Role : Role representation in access control levels
type Role struct {
	ID          int64                    `gorm:"AUTO_INCREMENT;column:id;primary_key" json:"-"`
	Name        string                   `gorm:"column:name;type:varchar;unique;not null" json:"name"`
	Permissions []permissions.Permission `gorm:"many2many:role_permissions" json:"permissions,omitempty"`
	Idx         int64                    `gorm:"column:idx;" json:"idx"`
}

// Override of default table name
func (Role) TableName() string {
	return "role_tb"
}
