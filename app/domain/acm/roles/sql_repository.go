package roles

import (
	"errors"
	"fmt"
	"strings"

	p "gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
)

type SQLRoleRepository struct {
	dataSource database.DataSource
}

func New(dataSource database.DataSource) RoleRepository {
	return &SQLRoleRepository{
		dataSource: dataSource,
	}
}

func (srr *SQLRoleRepository) Add(Role *Role) error {
	permissions := make([]string, 0)
	for _, permission := range Role.Permissions {
		permissions = append(permissions, permission.Name)
	}
	Role.Permissions = nil
	Role.Permissions = make([]p.Permission, 0)
	tx := srr.dataSource.Connection().Begin()
	if err := tx.Where("name IN (?)", permissions).Find(&Role.Permissions).Error; err != nil {
		tx.Rollback()
		return err
	}
	if len(Role.Permissions) == 0 {
		tx.Rollback()
		return errors.New(NO_PERMISSIONS_PRESENT)
	}

	if err := tx.Omit("idx").Create(Role).Error; err != nil {
		tx.Rollback()
		if strings.Contains(strings.ToLower(err.Error()), "unique") {
			return errors.New(NON_UNIQUE_ROLE)
		}
		return err
	}
	if err := tx.Model(Role).Association("Permissions").Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Model(Role).Association("Permissions").Append(Role.Permissions); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (srr *SQLRoleRepository) Update(Name string, AddedPermissions, RemovedPermissions []string) error {
	role := Role{}
	tx := srr.dataSource.Connection().Begin()
	if err := tx.Where("name = ?", Name).First(&role).Error; err != nil {
		return errors.New(ROLE_NOT_FOUND)
	}

	if len(AddedPermissions) > 0 {
		addedPermissions := make([]p.Permission, 0)
		if err := tx.Where("name IN (?)", AddedPermissions).Find(&addedPermissions).Error; err != nil {
			return err
		}
		if len(addedPermissions) == 0 {
			tx.Rollback()
			return errors.New(NO_PERMISSIONS_PRESENT)
		}
		if err := tx.Model(&role).Association("Permissions").Error; err != nil {
			tx.Rollback()
			return err
		}

		if err := tx.Model(&role).Association("Permissions").Append(addedPermissions); err != nil {
			tx.Rollback()
			return err
		}
	}
	if len(RemovedPermissions) > 0 {
		removedPermissions := make([]p.Permission, 0)
		if err := tx.Where("name IN (?)", RemovedPermissions).Find(&removedPermissions).Error; err != nil {
			return err
		}
		if len(removedPermissions) == 0 {
			fmt.Println("FOUND NO MATCHING PERMISSIONS TO REMOVE")
			tx.Rollback()
			return errors.New(NO_PERMISSIONS_PRESENT)
		}
		if err := tx.Model(&role).Association("Permissions").Error; err != nil {
			fmt.Println("FAILED TO DEAL WITH ASSOCIATION")
			tx.Rollback()
			return err
		}
		if err := tx.Model(&role).Association("Permissions").Delete(&removedPermissions); err != nil {
			fmt.Println("FAILED TO DISPOSE PERMISSIONS")
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}

func (srr *SQLRoleRepository) Search(Query string, Limit, Offset int64) ([]*Role, error) {
	if Limit == 0 {
		Limit = -1
	}
	roles := make([]*Role, 0)
	connection := srr.dataSource.Connection()
	if len(Query) > 0 {
		var sql = fmt.Sprintf("idx > %d AND name LIKE ?", Offset)
		if err := connection.
			Preload("Permissions").
			Order("name desc").
			Limit(int(Limit)).
			Where(sql, "%"+Query+"%").
			Find(&roles).Error; err != nil {
			return roles, err
		}
	} else {
		var sql = fmt.Sprintf("idx > %d", Offset)
		if err := connection.
			Preload("Permissions").
			Order("name desc").
			Limit(int(Limit)).
			Where(sql).
			Find(&roles).Error; err != nil {
			return roles, err
		}
	}
	return roles, nil
}

func (srr *SQLRoleRepository) Remove(Name string) error {
	tx := srr.dataSource.Connection().Begin()
	if err := tx.Unscoped().Where("name = ?", Name).Delete(Role{}).Error; err != nil {
		tx.Rollback()
		return errors.New(ROLE_NOT_FOUND)
	}
	return tx.Commit().Error
}

func (srr *SQLRoleRepository) Fetch(Exclude []string) ([]*Role, error) {
	roles := make([]*Role, 0)
	connection := srr.dataSource.Connection()
	if err := connection.
		Preload("Permissions").
		Order("name desc").
		Not(map[string]interface{}{"name": Exclude}).
		Find(&roles).Error; err != nil {
		return roles, err
	}
	return roles, nil
}
