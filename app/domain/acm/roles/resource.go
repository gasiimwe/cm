package roles

import (
	"net/http"

	"encoding/json"

	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/account-service/acm/roles"

type RolesController struct {
	*web.ControllerBase
	repository RoleRepository
}

func NewController(Repository RoleRepository) web.HTTPController {
	return &RolesController{repository: Repository}
}

func (rc *RolesController) Name() string {
	return "roles-controller"
}

func (rc *RolesController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"POST",
		rc.add,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		rc.search,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH+"/{name}",
		[]string{"SUPER_ADMINISTRATOR"},
		"PUT",
		rc.update,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH+"/{name}",
		[]string{"SUPER_ADMINISTRATOR"},
		"DELETE",
		rc.remove,
	)
}

func (rc *RolesController) add(w http.ResponseWriter, r *http.Request) {
	role := &Role{}
	if err := json.NewDecoder(r.Body).Decode(role); err != nil {
		rc.BadRequest(w, err)
	} else if err := rc.repository.Add(role); err != nil {
		rc.ServiceFailure(w, err)
	} else {
		rc.Created(w, "Role successfully created")
	}
}

func (rc *RolesController) search(w http.ResponseWriter, r *http.Request) {
	roles := make([]*Role, 0)
	query := rc.GetQueryString("q", r)
	limit := rc.GetQueryInt("limit", r)
	offset := rc.GetQueryInt("offset", r)
	roles, err := rc.repository.Search(
		query,
		limit,
		offset,
	)
	if err != nil {
		rc.ServiceFailure(w, err)
	} else {
		rc.OK(w, roles)
	}
}

func (rc *RolesController) update(w http.ResponseWriter, r *http.Request) {
	name := rc.GetVar("name", r)
	update := &RoleUpdate{}
	if err := json.NewDecoder(r.Body).Decode(update); err != nil {
		rc.BadRequest(w, err)
	} else if err := rc.repository.Update(name, update.AddedPermissions, update.RemovedPermissions); err != nil {
		rc.ServiceFailure(w, err)
	} else {
		rc.OKNoResponse(w)
	}
}

func (rc *RolesController) remove(w http.ResponseWriter, r *http.Request) {
	name := rc.GetVar("name", r)
	if err := rc.repository.Remove(name); err != nil {
		rc.ServiceFailure(w, err)
	} else {
		rc.OKNoResponse(w)
	}
}
