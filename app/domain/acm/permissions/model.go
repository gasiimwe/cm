package permissions

// Permission : Represents a User Permission `ACM`
type Permission struct {
	ID   int64  `gorm:"AUTO_INCREMENT;column:id;primary_key" json:"-"`
	Name string `gorm:"column:name;type:varchar;unique;not null" json:"name"`
	Idx  int64  `gorm:"column:idx;" json:"idx"`
}

// Override for the table name in `GORM-ORM`
func (Permission) TableName() string {
	return "permission_tb"
}
