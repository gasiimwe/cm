package permissions

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
)

type SQLPermissionsRepository struct {
	dataSource database.DataSource
}

func New(dataSource database.DataSource) PermissionRepository {
	return &SQLPermissionsRepository{
		dataSource: dataSource,
	}
}

func (spr *SQLPermissionsRepository) Add(Permissions []Permission) error {

	if len(Permissions) == 0 {
		return errors.New(NO_PERMISSIONS_SUBMITTED)
	}
	valueEntries := []string{}
	valueArguments := []interface{}{}

	for _, permission := range Permissions {
		valueEntries = append(valueEntries, "(?)")
		valueArguments = append(valueArguments, permission.Name)
	}

	var sql = `INSERT INTO permission_tb(name) VALUES %s`

	sql = fmt.Sprintf(sql, strings.Join(valueEntries, ","))
	tx := spr.dataSource.Connection().Begin()
	if err := tx.Exec(sql, valueArguments...).Error; err != nil {
		tx.Rollback()
		if strings.Contains(strings.ToLower(err.Error()), "unique") {
			return errors.New(DUPLICATE_ERROR)
		}
		return err
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (spr *SQLPermissionsRepository) Search(Query string, Limit, Offset int64) ([]*Permission, error) {
	permissions := make([]*Permission, 0)
	//TODO : Add the SQL Query for the permissions
	var sql string
	if Limit == 0 {
		Limit = -1
	}
	if Offset == 0 {
		Offset = -1
	}
	connection := spr.dataSource.Connection()
	if len(Query) != 0 {
		sql = fmt.Sprintf("id > %d AND name LIKE ? ", Offset)
		if err := connection.
			Order("name desc").
			Limit(int(Limit)).
			Where(sql, "%"+Query+"%").
			Find(&permissions).Error; err != nil {
			return permissions, err
		}
	} else {
		sql = fmt.Sprintf("id > %d", Offset)
		if err := connection.
			Order("name desc").
			Limit(int(Limit)).
			Where(sql).
			Find(&permissions).Error; err != nil {
			return permissions, err
		}
	}
	return permissions, nil
}

func (spr *SQLPermissionsRepository) Remove(Permissions []string) error {
	tx := spr.dataSource.Connection().Begin()
	if err := tx.Unscoped().Where("name IN (?)", Permissions).Delete(&Permission{}).Error; err != nil {
		tx.Rollback()
		return errors.New(NONE_EXISTENT_ERROR)
	}
	return tx.Commit().Error
}
