package permissions_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestPermissionsRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Permissions Repository Suite")
}

type testDataSource struct {
	db *gorm.DB
}

func (tds *testDataSource) Connection() *gorm.DB {
	if tds.db == nil {
		var err error
		tds.db, err = gorm.Open(
			sqlite.Open("file::memory:?mode=memory&cache=shared"),
			&gorm.Config{})
		if err != nil {
			panic(err)
		}
	}
	return tds.db
}

func (tds *testDataSource) Close() error {
	if tds.db != nil {
		return tds.Close()
	}
	return nil
}

func dataSourceProvider() database.DataSource {
	return &testDataSource{}
}

func migrate(dataSource database.DataSource) {
	sql := `CREATE TRIGGER IF NOT EXISTS update_permission_idx AFTER INSERT ON permission_tb
		   BEGIN
			UPDATE permission_tb SET idx=id WHERE id=NEW.id;
		   END;`
	dataSource.Connection().AutoMigrate(&permissions.Permission{})
	dataSource.Connection().Exec(sql)
}

var _ = Describe("Permission store tests", func() {

	Context("Given a unique permission is added", func() {
		dataSource := dataSourceProvider()
		migrate(dataSource)
		permissionsRepository := permissions.New(dataSource)
		BeforeEach(func() {
			permissionsRepository.Add([]permissions.Permission{{Name: "CAN_POPULATE_CART"}})
		})
		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM permission_tb")
		})

		It("The permission can be retrieved", func() {
			results, err := permissionsRepository.Search("CAN_POPULATE_CART", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeEmpty())
			Expect(results[0].Name).To(Equal("CAN_POPULATE_CART"))
		})
		It("The same permission cannot be added", func() {
			err := permissionsRepository.Add([]permissions.Permission{{Name: "CAN_POPULATE_CART"}})
			Expect(err).To(HaveOccurred())
		})
		It("The permission can be searched for partially", func() {
			results, err := permissionsRepository.Search("POP", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeNil())
			Expect(results[0].Name).To(Equal("CAN_POPULATE_CART"))
		})
		It("The permission can be removed", func() {
			err := permissionsRepository.Remove([]string{"CAN_POPULATE_CART"})
			Expect(err).ToNot(HaveOccurred())
			results, err := permissionsRepository.Search("CAN_POPULATE_CART", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).To(BeEmpty())
		})
	})

})
