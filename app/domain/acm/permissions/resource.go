package permissions

import (
	"net/http"

	"encoding/json"

	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/account-service/acm/permissions"

type PermissionsController struct {
	*web.ControllerBase
	repository PermissionRepository
}

func NewController(Repository PermissionRepository) web.HTTPController {
	return &PermissionsController{repository: Repository}
}

func (pc *PermissionsController) Name() string {
	return "permissions-controller"
}

func (pc *PermissionsController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"POST",
		pc.add,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		pc.search,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"DELETE",
		pc.remove,
	)
}

func (pc *PermissionsController) add(w http.ResponseWriter, r *http.Request) {
	permissions := make([]Permission, 0)
	if err := json.NewDecoder(r.Body).Decode(&permissions); err != nil {
		pc.BadRequest(w, err)
	} else if err := pc.repository.Add(permissions); err != nil {
		pc.ServiceFailure(w, err)
	} else {
		pc.Created(w, "permissions successfully added")
	}
}

func (pc *PermissionsController) search(w http.ResponseWriter, r *http.Request) {
	query := pc.GetQueryString("q", r)
	limit := pc.GetQueryInt("limit", r)
	offset := pc.GetQueryInt("offset", r)
	results, err := pc.repository.Search(
		query,
		limit,
		offset,
	)
	if err != nil {
		pc.ServiceFailure(w, err)
	} else {
		pc.OK(w, results)
	}
}

func (pc *PermissionsController) remove(w http.ResponseWriter, r *http.Request) {
	permissions := make([]string, 0)
	if err := json.NewDecoder(r.Body).Decode(&permissions); err != nil {
		pc.BadRequest(w, err)
	} else if err := pc.repository.Remove(permissions); err != nil {
		pc.ServiceFailure(w, err)
	} else {
		pc.OKNoResponse(w)
	}
}
