package permissions_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestPermissionsResource(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Permissions Resource Suite")
}

var _ = Describe("Permission resource tests", func() {

})
