package permissions

const (
	DUPLICATE_ERROR          = "PERMISSIONS ALREADY EXIST"
	NONE_EXISTENT_ERROR      = "PERMISSIONS DO NOT EXIST"
	NO_PERMISSIONS_SUBMITTED = "NO PERMISSIONS HAVE BEEN SUBMITTED"
)

// PermissionRepository : Contract for the operations
type PermissionRepository interface {
	Add(Permissions []Permission) error
	Search(Query string, Limit, Offset int64) ([]*Permission, error)
	Remove(Permissions []string) error
}
