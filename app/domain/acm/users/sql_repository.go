package users

import (
	"errors"
	"strings"

	uuid "github.com/satori/go.uuid"
	r "gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
)

type SQLUserRepository struct {
	dataSource database.DataSource
}

func New(dataSource database.DataSource) UserRepository {
	return &SQLUserRepository{
		dataSource: dataSource,
	}
}

func (sur *SQLUserRepository) Add(User *User, Roles []string) error {
	tx := sur.dataSource.Connection().Begin()
	User.Roles = nil
	if err := tx.Where("name IN (?)", Roles).Find(&User.Roles).Error; err != nil {
		tx.Rollback()
		return err
	}
	if len(User.Roles) == 0 {
		tx.Rollback()
		return errors.New(NO_ROLES_FOUND)
	}
	if err := tx.Omit("idx").Create(User).Error; err != nil {
		if strings.Contains(strings.ToLower(err.Error()), "unique") {
			tx.Rollback()
			return errors.New(MATCHING_USER_EXISTS)
		}
		return err
	}
	if err := tx.Model(User).Association("Roles").Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Model(User).Association("Roles").Append(User.Roles); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
func (sur *SQLUserRepository) FetchByUID(UID uuid.UUID) (*User, error) {
	user := &User{}
	connection := sur.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("uid = ?", UID).
		First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}
func (sur *SQLUserRepository) FetchUsersByUIDs(UIDs []string) ([]*User, error) {
	users := make([]*User, 0)
	connection := sur.dataSource.Connection()
	if err := connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Order("first_name ASC").
		Where("uid IN (?)", UIDs).
		Find(&users).Error; err != nil {
		return users, err
	}
	return users, nil
}
func (sur *SQLUserRepository) FetchUserByPhoneNumber(PhoneNumber string) (*User, error) {
	user := &User{}
	connection := sur.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("phone_number = ?", PhoneNumber).First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}
func (sur *SQLUserRepository) FetchUserByEmailAddress(EmailAddress string) (*User, error) {
	user := &User{}
	connection := sur.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("email_address = ?", EmailAddress).First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}
func (sur *SQLUserRepository) FetchUserByIdNumber(IdType, IdNumber string) (*User, error) {
	user := &User{}
	connection := sur.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("id_type = ? AND id_number = ?", IdType, IdNumber).First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}
func (sur *SQLUserRepository) FetchUserByUsername(Username string) (*User, error) {
	user := &User{}
	connection := sur.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("username = ?", Username).
		First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}

// Management Specific
func (sur *SQLUserRepository) Update(TargetUser *User, AddedRoles, RemovedRoles []string) error {
	tx := sur.dataSource.Connection().Begin()
	previousUser := &User{}
	if err := tx.Where("uid = ?", TargetUser.UID).First(previousUser).Error; err != nil {
		tx.Rollback()
		return err
	}
	if len(AddedRoles) > 0 {
		addedRoles := make([]r.Role, 0)
		if err := tx.Where("name IN (?)", AddedRoles).Find(&addedRoles).Error; err != nil {
			tx.Rollback()
			return err
		}
		if len(addedRoles) == 0 {
			tx.Rollback()
			return errors.New(NO_ROLES_FOUND)
		}
		if err := tx.Model(previousUser).Association("Roles").Error; err != nil {
			tx.Rollback()
			return err
		}
		if err := tx.Model(previousUser).Association("Roles").Append(addedRoles); err != nil {
			tx.Rollback()
			return err
		}
	}
	if len(RemovedRoles) > 0 {
		removedRoles := make([]r.Role, 0)
		if err := tx.Where("name IN (?)", RemovedRoles).Find(&removedRoles).Error; err != nil {
			tx.Rollback()
			return err
		}
		if len(removedRoles) == 0 {
			tx.Rollback()
			return errors.New(NO_ROLES_FOUND)
		}
		if err := tx.Model(previousUser).Association("Roles").Error; err != nil {
			tx.Rollback()
			return err
		}
		if err := tx.Model(previousUser).Association("Roles").Delete(removedRoles); err != nil {
			tx.Rollback()
			return err
		}
	}
	tx.Model(previousUser)
	if err := tx.Model(previousUser).Omit("idx", "uid").Updates(TargetUser).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
func (sur *SQLUserRepository) Search(Query string, Limit, Offset int64) ([]*User, error) {
	if Limit == 0 {
		Limit = -1
	}

	users := make([]*User, 0)
	connection := sur.dataSource.Connection()
	if len(Query) > 0 {
		var sql = "idx > ? AND first_name LIKE ? OR last_name LIKE ? OR email_address LIKE ? OR phone_number LIKE ? OR username LIKE ?"
		var q = "%" + Query + "%"
		if err := connection.
			Preload("Roles").
			Order("first_name asc").
			Limit(int(Limit)).
			Where(sql, Offset, q, q, q, q, q).
			Find(&users).Error; err != nil {
			return users, err
		}
	} else {
		var sql = "idx > ?"
		if err := connection.
			Preload("Roles").
			Order("first_name asc").
			Limit(int(Limit)).
			Where(sql, Offset).
			Find(&users).Error; err != nil {
			return users, err
		}
	}
	return users, nil
}
func (sur *SQLUserRepository) Remove(UserReference uuid.UUID) error {
	tx := sur.dataSource.Connection().Begin()
	var sql = "DELETE FROM user_tb WHERE uid = ?"
	if err := tx.Exec(sql, UserReference).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (sur *SQLUserRepository) StaffSearch(Query string, Limit, Offset int64) ([]*User, error) {
	if Limit == 0 {
		Limit = -1
	}
	exlude := map[string]bool{
		"SUPER_ADMINISTRATOR": true,
		"APPLICANT":           true,
	}
	users := make([]*User, 0)
	filteredUsers := make([]*User, 0)
	connection := sur.dataSource.Connection()
	if len(Query) > 0 {
		var sql = "idx > ? AND first_name LIKE ? OR last_name LIKE ? OR " +
			"email_address LIKE ? OR phone_number LIKE ? OR " +
			"username LIKE ? OR organization LIKE ?"
		var q = "%" + Query + "%"
		if err := connection.
			Preload("Roles").
			Order("first_name ASC").
			Limit(int(Limit)).
			Where(sql, Offset, q, q, q, q, q, q).
			Find(&users).Error; err != nil {
			return users, err
		}
	} else {
		var sql = "idx > ?"
		if err := connection.
			Preload("Roles").
			Order("first_name ASC").
			Limit(int(Limit)).
			Where(sql, Offset).
			Find(&users).Error; err != nil {
			return users, err
		}
	}
	for _, user := range users {
		isFiltered := true
		for _, role := range user.Roles {
			if exlude[role.Name] {
				isFiltered = false
			}
		}
		if isFiltered {
			filteredUsers = append(filteredUsers, user)
		}
	}
	return users, nil
}

func (sur *SQLUserRepository) FetchDetailsByUID(UID string) (*DetailContent, error) {
	details := &DetailContent{}
	var sql = "SELECT uid, first_name, last_name, email_address, phone_number FROM user_tb WHERE uid = ?"
	connection := sur.dataSource.Connection()
	if err := connection.Raw(sql, UID).First(details).Error; err != nil {
		return details, err
	}
	return details, nil
}

func (sur *SQLUserRepository) FetchDetailsByRole(Role string) ([]*DetailContent, error) {
	details := make([]*DetailContent, 0)
	var sql = "SELECT u.uid, u.first_name, u.last_name, u.email_address, u.phone_number " +
		"FROM user_tb AS u " +
		"INNER JOIN user_roles AS ur ON ur.user_id = u.id " +
		"INNER JOIN role_tb AS r ON r.id = ur.role_id " +
		"WHERE r.name = ?"
	connection := sur.dataSource.Connection()
	if err := connection.Raw(sql, Role).Find(&details).Error; err != nil {
		return details, err
	}
	return details, nil
}
