package users

import (
	"net/http"

	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/cm/acm/users"

type UsersController struct {
	*web.ControllerBase
	repository UserRepository
}

func NewController(Repository UserRepository) web.HTTPController {
	return &UsersController{repository: Repository}
}

func (uc *UsersController) Name() string {
	return "users-controller"
}

func (uc *UsersController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		uc.search,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH+"/{id-type}/id-number",
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		uc.fetchByIdentityCard,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH+"/batch",
		[]string{"SUPER_ADMINISTRATOR"},
		"POST",
		uc.fetchBatchByIds,
	)
	RouteRegistry.Add(
		fmt.Sprintf("%s/{identifier:%s}", BASE_PATH, web.GUID_PATTERN),
		true,
		"GET",
		uc.fetchByIdentifier,
	)
}

func (uc *UsersController) search(w http.ResponseWriter, r *http.Request) {
	query := uc.GetQueryString("q", r)
	limit := uc.GetQueryInt("limit", r)
	offset := uc.GetQueryInt("offset", r)
	users, err := uc.repository.Search(
		query,
		limit,
		offset,
	)
	if err != nil {
		uc.ServiceFailure(w, err)
	} else {
		uc.OK(w, users)
	}
}

func (uc *UsersController) fetchByIdentifier(w http.ResponseWriter, r *http.Request) {
	identifier := uc.GetVar("identifier", r)
	user, err := uc.repository.FetchByUID(uuid.FromStringOrNil(identifier))
	if err != nil {
		uc.ServiceFailure(w, err)
	} else {
		uc.OK(w, user)
	}
}

func (uc *UsersController) fetchByIdentityCard(w http.ResponseWriter, r *http.Request) {
	idType := uc.GetVar("id-type", r)
	idNumber := uc.GetVar("id-number", r)
	user, err := uc.repository.FetchUserByIdNumber(idType, idNumber)
	if err != nil {
		uc.ServiceFailure(w, err)
	} else {
		uc.OK(w, user)
	}
}

func (uc *UsersController) fetchBatchByIds(w http.ResponseWriter, r *http.Request) {
	ids := make([]string, 0)
	if err := json.NewDecoder(r.Body).Decode(&ids); err != nil {
		uc.BadRequest(w, err)
	} else {
		users, err := uc.repository.FetchUsersByUIDs(ids)
		if err != nil {
			uc.ServiceFailure(w, err)
		} else {
			uc.OK(w, users)
		}
	}
}
