package users

import (
	uuid "github.com/satori/go.uuid"
)

const (
	NO_ROLES_FOUND       = "NO MATCHING ROLES FOUND"
	MATCHING_USER_EXISTS = "MATCHING USER ALREADY EXISTS"
	UNKNOWN_USER         = "NO MATCHING USER FOUND"
)

// UserRepository : UserRepository contract
type UserRepository interface {
	Add(User *User, Roles []string) error
	FetchByUID(UID uuid.UUID) (*User, error)
	FetchUsersByUIDs(UIDs []string) ([]*User, error)
	FetchUserByPhoneNumber(PhoneNumber string) (*User, error)
	FetchUserByEmailAddress(EmailAddress string) (*User, error)
	FetchUserByIdNumber(IdType, IdNumber string) (*User, error)
	FetchUserByUsername(Username string) (*User, error)
	// Management Specific
	Update(TargetUser *User, AddedRoles, RemovedRoles []string) error
	Search(Query string, Limit, Offset int64) ([]*User, error)
	Remove(UserReference uuid.UUID) error
	// Staff Specific
	StaffSearch(Query string, Limit, Offset int64) ([]*User, error)
	FetchDetailsByUID(UID string) (*DetailContent, error)
	FetchDetailsByRole(Role string) ([]*DetailContent, error)
}
