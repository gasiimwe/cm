package users_test

import (
	"fmt"
	"testing"

	"database/sql"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestUsersRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Users Repository Suite")
}

type testDataSource struct {
	db *gorm.DB
}

func (tds *testDataSource) Connection() *gorm.DB {
	if tds.db == nil {
		var err error
		tds.db, err = gorm.Open(
			sqlite.Open("file::memory:?mode=memory&cache=shared"),
			&gorm.Config{})
		if err != nil {
			panic(err)
		}
	}
	return tds.db
}

func (tds *testDataSource) Close() error {
	if tds.db != nil {
		return tds.Close()
	}
	return nil
}

func dataSourceProvider() database.DataSource {
	return &testDataSource{}
}

func migrate(dataSource database.DataSource) {
	rule0 := `CREATE TRIGGER IF NOT EXISTS update_permission_idx AFTER INSERT ON permission_tb
		   BEGIN
			UPDATE permission_tb SET idx=id WHERE id=NEW.id;
		   END;`

	rule1 := `CREATE TRIGGER update_role_idx AFTER INSERT ON role_tb
			  BEGIN
			   UPDATE role_tb SET idx=id WHERE id=NEW.id;
			  END;`
	rule2 := `CREATE TRIGGER update_user_idx AFTER INSERT ON user_tb
			  BEGIN
			   UPDATE user_tb SET idx=id WHERE id=NEW.id;
			  END;`

	dataSource.Connection().AutoMigrate(&permissions.Permission{}, &roles.Role{}, &users.User{})
	dataSource.Connection().Exec(rule0)
	dataSource.Connection().Exec(rule1)
	dataSource.Connection().Exec(rule2)
}

func addRole(dataSource database.DataSource, RoleName string, addedPermissions []string) {
	added := make([]permissions.Permission, 0)
	for _, permission := range addedPermissions {
		added = append(added, permissions.Permission{Name: permission})
	}
	permissions.New(dataSource).Add(added)
	roles.New(dataSource).Add(&roles.Role{Name: RoleName, Permissions: added})
}

var _ = Describe("User store tests", func() {
	dataSource := dataSourceProvider()
	migrate(dataSource)
	userRepository := users.New(dataSource)
	Context("Given an existing role", func() {
		BeforeEach(func() {
			addRole(dataSource, "CUSTOMER", []string{"CAN_CREATE_CART", "CAN_ADD_TO_CART"})
		})

		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM user_tb")
			dataSource.Connection().Exec("DELETE FROM role_tb")
		})

		It("A user can be added", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: true,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())

		})

		It("A user can be retrieved by partial search", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			results, err := userRepository.Search("Ar", 1, 0)
			Expect(err).ToNot(HaveOccurred())
			Expect(results).ToNot(BeEmpty())
			Expect(results[0].FirstName).To(Equal("Arham"))
			Expect(results[0].LastName).To(Equal("Shazad"))
			Expect(results[0].Activated).To(BeFalse())
			Expect(results[0].Roles).ToNot(BeEmpty())
			Expect(results[0].Roles[0].Name).To(Equal("CUSTOMER"))
		})

		It("A user can be retrieved by uid", func() {
			uid := uuid.NewV4()
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uid},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchByUID(uid)
			Expect(err).ToNot(HaveOccurred())
			Expect(user.FirstName).To(Equal("Arham"))
			Expect(user.LastName).To(Equal("Shazad"))
			Expect(user.Activated).To(BeFalse())
			Expect(user.Roles).ToNot(BeEmpty())
			Expect(user.Roles[0].Name).To(Equal("CUSTOMER"))
		})

		It("A user can be retrieved by username", func() {

			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchUserByUsername("NShazard")
			Expect(err).ToNot(HaveOccurred())
			Expect(user.FirstName).To(Equal("Arham"))
			Expect(user.LastName).To(Equal("Shazad"))
			Expect(user.Activated).To(BeFalse())
			Expect(user.Roles).ToNot(BeEmpty())
			Expect(user.Roles[0].Name).To(Equal("CUSTOMER"))
		})

		It("A user can be retrieved by phone number", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchUserByPhoneNumber("+256774921350")
			Expect(err).ToNot(HaveOccurred())
			Expect(user.FirstName).To(Equal("Arham"))
			Expect(user.LastName).To(Equal("Shazad"))
			Expect(user.Activated).To(BeFalse())
			Expect(user.Roles).ToNot(BeEmpty())
			Expect(user.Roles[0].Name).To(Equal("CUSTOMER"))
		})

		It("A user can be retrieved by email", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchUserByEmailAddress("arham.shazad@gmail.com")
			Expect(err).ToNot(HaveOccurred())
			Expect(user.FirstName).To(Equal("Arham"))
			Expect(user.LastName).To(Equal("Shazad"))
			Expect(user.Activated).To(BeFalse())
			Expect(user.Roles).ToNot(BeEmpty())
			Expect(user.Roles[0].Name).To(Equal("CUSTOMER"))
		})
		It("A user can be retrieved by id number", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				IdType:    sql.NullString{String: "NATIONAL_ID", Valid: true},
				IdNumber:  sql.NullString{String: "C90045908909484938", Valid: true},
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchUserByIdNumber("NATIONAL_ID", "C90045908909484938")
			Expect(err).ToNot(HaveOccurred())
			Expect(user.FirstName).To(Equal("Arham"))
			Expect(user.LastName).To(Equal("Shazad"))
			Expect(user.Activated).To(BeFalse())
			Expect(user.Roles).ToNot(BeEmpty())
			Expect(user.Roles[0].Name).To(Equal("CUSTOMER"))
		})
	})

	Context("Given an existing user", func() {

		BeforeEach(func() {
			addRole(dataSource, "CUSTOMER", []string{"CAN_CREATE_CART", "CAN_ADD_TO_CART"})
			userRepository.Add(&users.User{
				FirstName: "Arham",
				LastName:  "Shazad",
				Gender:    "MALE",
				Email:     "arham.shazad@gmail.com",
				Phone:     "+256774921350",
				IdType:    sql.NullString{String: "NATIONAL_ID", Valid: true},
				IdNumber:  sql.NullString{String: "C90045908909484938", Valid: true},
				UserName:  "NShazard",
				Password:  "bitx5n20*",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
		})

		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM user_tb")
			dataSource.Connection().Exec("DELETE FROM role_tb")
		})

		It("A user with existing username cannot be created", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Alex",
				LastName:  "Asiimwe",
				Gender:    "MALE",
				Email:     "alex.asiimwe@gmail.com",
				Phone:     "+256794225908",
				IdType:    sql.NullString{String: "PASSPORT", Valid: true},
				IdNumber:  sql.NullString{String: "BU459089", Valid: true},
				UserName:  "NShazard",
				Password:  "bin%io90",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(users.MATCHING_USER_EXISTS))
		})

		It("A user with an existing phone number cannot be created", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Alex",
				LastName:  "Asiimwe",
				Gender:    "MALE",
				Email:     "alex.asiimwe@gmail.com",
				Phone:     "+256774921350",
				IdType:    sql.NullString{String: "PASSPORT", Valid: true},
				IdNumber:  sql.NullString{String: "BU459089", Valid: true},
				UserName:  "alex.mex",
				Password:  "bin%io90",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(users.MATCHING_USER_EXISTS))
		})

		It("A user with an existing Id number cannot be created", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Alex",
				LastName:  "Asiimwe",
				Gender:    "MALE",
				Email:     "alex.asiimwe@gmail.com",
				Phone:     "+256798445908",
				IdType:    sql.NullString{String: "NATIONAL_ID", Valid: true},
				IdNumber:  sql.NullString{String: "C90045908909484938", Valid: true},
				UserName:  "alex.mex",
				Password:  "bin%io90",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(users.MATCHING_USER_EXISTS))
		})

		It("A user cannot be updated with a role that does not exist", func() {
			err := userRepository.Add(&users.User{
				FirstName: "Alex",
				LastName:  "Asiimwe",
				Gender:    "MALE",
				Email:     "alex.asiimwe@gmail.com",
				Phone:     "+256798445908",
				IdType:    sql.NullString{String: "PASSPORT", Valid: true},
				IdNumber:  sql.NullString{String: "BU459089", Valid: true},
				UserName:  "alex.mex",
				Password:  "bin%io90",
				Audience:  "customer",
				Activated: false,
				UID:       uuid.NewV4()},
				[]string{"CUSTOMER"})
			Expect(err).ToNot(HaveOccurred())
			user, err := userRepository.FetchUserByPhoneNumber("+256798445908")
			Expect(err).ToNot(HaveOccurred())
			err = userRepository.Update(user, []string{"SHOP_LIFTER"}, []string{})
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal(users.NO_ROLES_FOUND))
		})

		Context("Given an additional role and an existing user", func() {
			var user *users.User
			BeforeEach(func() {
				addRole(dataSource, "SHOP_LIFTER", []string{"CAN_STEAL_CART"})
				user = &users.User{
					FirstName: "Lies",
					LastName:  "MoreLies",
					Gender:    "FEMALE",
					Email:     "lies.lair@gmail.com",
					Phone:     "+25970897609",
					IdType:    sql.NullString{String: "PASSPORT", Valid: true},
					IdNumber:  sql.NullString{String: "BK459189", Valid: true},
					UserName:  "lin.thio",
					Password:  "lin%09",
					Audience:  "customer",
					Activated: false,
					UID:       uuid.NewV4()}
				userRepository.Add(user,
					[]string{"CUSTOMER"})
			})

			AfterEach(func() {
				dataSource.Connection().Exec("DELETE FROM user_tb")
				dataSource.Connection().Exec("DELETE FROM role_tb")
			})

			It("A user can be updated with an additional role", func() {
				err := userRepository.Update(user, []string{"SHOP_LIFTER"}, []string{})
				Expect(err).ToNot(HaveOccurred())
				user, err := userRepository.FetchByUID(user.UID)
				Expect(err).ToNot(HaveOccurred())
				Expect(len(user.Roles)).To(Equal(2))
			})

			It("A user can have a role removed", func() {
				err := userRepository.Update(user, []string{"SHOP_LIFTER"}, []string{"CUSTOMER"})
				Expect(err).ToNot(HaveOccurred())
				user, err := userRepository.FetchByUID(user.UID)
				Expect(err).ToNot(HaveOccurred())
				Expect(len(user.Roles)).To(Equal(1))
				Expect(user.Roles[0].Name).To(Equal("SHOP_LIFTER"))
			})
		})
	})

	Context("Given multiple users are created", func() {
		ids := make([]string, 0)
		BeforeEach(func() {
			addRole(dataSource, "CUSTOMER", []string{"CAN_CREATE_CART", "CAN_ADD_TO_CART"})

			for _, idx := range []int{1, 2, 3} {
				user := &users.User{
					FirstName: fmt.Sprintf("Arham%d", idx),
					LastName:  fmt.Sprintf("Shazad%d", idx),
					Gender:    "MALE",
					Email:     fmt.Sprintf("arham.shazad%d@gmail.com", idx),
					Phone:     fmt.Sprintf("+25677892135%d", idx),
					UserName:  fmt.Sprintf("NShazard%d", idx),
					Password:  fmt.Sprintf("bitx5n20*%d", idx),
					Audience:  "customer",
					Activated: false,
					UID:       uuid.NewV4(),
				}

				ids = append(ids, user.UID.String())
				userRepository.Add(user, []string{"CUSTOMER"})
			}
		})

		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM user_tb")
			dataSource.Connection().Exec("DELETE FROM role_tb")
		})

		It("Can fetch a batch of users by uids", func() {
			users, err := userRepository.FetchUsersByUIDs(ids)
			Expect(err).NotTo(HaveOccurred())
			Expect(len(ids)).To(Equal(len(users)))
		})
	})

})
