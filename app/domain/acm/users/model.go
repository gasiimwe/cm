package users

import (
	"database/sql"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
)

// User : Represents what a system user is
type User struct {
	ID           int64          `gorm:"AUTO_INCREMENT;column:id;primary_key" json:"-"`
	FirstName    string         `gorm:"column:first_name;type:varchar;not null" json:"first_name"`
	LastName     string         `gorm:"column:last_name;type:varchar;not null" json:"last_name"`
	Gender       string         `gorm:"column:gender;type:varchar;not null" json:"gender"`
	IdType       sql.NullString `gorm:"column:id_type;type:varchar;null;uniqueIndex:idx_identification" json:"id_type,omitempty"`
	IdNumber     sql.NullString `gorm:"column:id_number;type:varchar;null;uniqueIndex:idx_identification" json:"id_number,omitempty"`
	Email        string         `gorm:"column:email_address;type:varchar;unique;not null" json:"email_address"`
	Phone        string         `gorm:"column:phone_number;type:varchar;unique;not null" json:"phone_number"`
	UserName     string         `gorm:"column:username;type:varchar;unique;not null" json:"username"`
	Password     string         `gorm:"column:password;type:varchar;unique;not null" json:"-"`
	Roles        []roles.Role   `gorm:"many2many:user_roles" json:"roles"`
	UID          uuid.UUID      `gorm:"column:uid;type:uuid;unique;not null" json:"uid"`
	Audience     string         `gorm:"column:audience;type:varchar;not null" json:"-"`
	Organization string         `gorm:"column:organization;type:varchar;null" json:"organization"`
	PhotoID      string         `gorm:"column:photo_reference;type:varchar;null" json:"photo_reference"`
	Activated    bool           `gorm:"column:activated;" json:"activated"`
	Idx          int64          `gorm:"column:idx;" json:"idx"`
	About        string         `gorm:"column:about;type:text" json:"about"`
}

func (User) TableName() string {
	return "user_tb"
}

type DetailContent struct {
	FirstName string `gorm:"column:first_name"`
	LastName  string `gorm:"column:last_name"`
	UID       string `gorm:"column:uid"`
	Email     string `gorm:"column:email_address"`
	Phone     string `gorm:"column:phone_number"`
}
