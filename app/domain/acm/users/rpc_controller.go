package users

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	nats "github.com/nats-io/nats.go"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users/details"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/rpc"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

type UserRPCController struct {
	repository UserRepository
}

func NewUserRPCController(repository UserRepository) rpc.NatController {
	return &UserRPCController{repository: repository}
}

func (uc *UserRPCController) Name() string {
	return "user-rpc-controller"
}

func (uc *UserRPCController) Initialize(Registry registry.TopicRegistry) {
	Registry.Add("v1/cm/fetch-details", uc.fetchDetails)
	Registry.Add("v1/cm/batch-identities", uc.fetchIdentities)
}

func (uc *UserRPCController) fetchDetails(handler *nats.Msg) {
	request := &details.DetailRequest{}
	if err := proto.Unmarshal(handler.Data, request); err != nil {
		uc.Failure(handler, err)
	} else if request.GetType() == details.DetailRequest_ROLE {
		results, err := uc.repository.FetchDetailsByRole(request.GetValue())
		if err != nil {
			uc.Failure(handler, err)
		} else {
			uc.Success(handler, results)
		}
	} else if request.GetType() == details.DetailRequest_UID {
		result, err := uc.repository.FetchDetailsByUID(request.GetValue())
		if err != nil {
			uc.Failure(handler, err)
		} else {
			results := make([]*DetailContent, 0)
			results = append(results, result)
			uc.Success(handler, results)
		}
	}
}

func (uc *UserRPCController) fetchIdentities(handler *nats.Msg) {
	request := &details.IdentityBatchRequest{}
	if err := proto.Unmarshal(handler.Data, request); err != nil {
		uc.Failure(handler, err)
	} else if len(request.GetIdentifiers()) == 0 {
		uc.Failure(handler, fmt.Errorf("No content in request"))
	} else {
		results, err := uc.repository.FetchUsersByUIDs(request.GetIdentifiers())
		if err != nil {
			uc.Failure(handler, err)
		} else {
			uc.batchIdentitySuccess(handler, results)
		}
	}
}

func (uc *UserRPCController) batchIdentitySuccess(rw *nats.Msg, users []*User) {
	response := &details.IdentityBatchResponse{}
	response.Identities = make([]*details.Identity, 0)
	for _, user := range users {
		roles := make([]string, 0)
		for _, role := range user.Roles {
			roles = append(roles, role.Name)
		}
		response.Identities = append(response.Identities, &details.Identity{
			FirstName:  user.FirstName,
			LastName:   user.LastName,
			MiddleName: "",
			Roles:      roles,
			Email:      user.Email,
			Phone:      user.Phone,
		})
	}
	data, err := proto.Marshal(response)
	if err != nil {
		uc.Failure(rw, err)
	} else {
		if err := rw.Respond(data); err != nil {
			logging.GetInstance().Error(err)
		}
	}
}

func (uc *UserRPCController) Success(rw *nats.Msg, results []*DetailContent) {
	response := &details.DetailResponse{}
	response.Details = make([]*details.Detail, 0)
	for _, entry := range results {
		response.Details = append(response.Details, &details.Detail{
			Uid:       entry.UID,
			FirstName: entry.FirstName,
			LastName:  entry.LastName,
			Email:     entry.Email,
			Phone:     entry.Phone,
		})
	}
	data, err := proto.Marshal(response)
	if err != nil {
		uc.Failure(rw, err)
	} else {
		if err := rw.Respond(data); err != nil {
			logging.GetInstance().Error(err)
		}
	}
}

func (uc *UserRPCController) Failure(rw *nats.Msg, failure error) {
	logging.GetInstance().Error(failure)
}
