package registration

import (
	"bytes"
	"errors"
	"fmt"
	"strings"

	"github.com/rivo/tview"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/domain/operations/base"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
)

type RegistrationDetails struct {
	FirstName       string
	LastName        string
	Username        string
	Password        string
	ConfirmPassword string
	EmailAddress    string
	PhoneNumber     string
}

func (rd *RegistrationDetails) Validate() error {
	var buffer bytes.Buffer

	if len(rd.FirstName) == 0 {
		buffer.WriteString("First name missing\n")
	}

	if len(rd.LastName) == 0 {
		buffer.WriteString("Last name missing\n")
	}
	if len(rd.Username) == 0 {
		buffer.WriteString("Username missing\n")
	}
	if len(rd.Password) == 0 {
		buffer.WriteString("Password missing\n")
	}
	if len(rd.ConfirmPassword) == 0 {
		buffer.WriteString("Confirm password missing\n")
	}
	if len(rd.EmailAddress) == 0 {
		buffer.WriteString("Email address missing\n")
	}
	if len(rd.PhoneNumber) == 0 {
		buffer.WriteString("Phone Number missing\n")
	}
	if strings.Compare(rd.Password, rd.ConfirmPassword) != 0 {
		buffer.WriteString("Passwords are not a match\n")
	}
	bugs := buffer.String()
	if len(bugs) > 0 {
		notice := fmt.Sprintf("Details are invalid:\n %s", bugs)
		return errors.New(notice)
	}
	return nil
}

func (rd *RegistrationDetails) ToUser(UID uuid.UUID, Password string) users.User {
	return users.User{
		UID:       UID,
		FirstName: rd.FirstName,
		LastName:  rd.LastName,
		UserName:  rd.Username,
		Password:  Password,
		Email:     rd.EmailAddress,
		Phone:     rd.PhoneNumber,
		Activated: true,
	}
}

type View interface {
	OnSuccess()
	OnFailure(Error error)
	OnBack()
}

type Actions interface {
	RegisterAdministrator(Details *RegistrationDetails)
}

type RegistrationPresenter struct {
	View            View
	UserRepository  users.UserRepository
	PasswordHandler security.PasswordHandler
}

func (rp *RegistrationPresenter) RegisterAdministrator(Details *RegistrationDetails) {
	if err := Details.Validate(); err != nil {
		rp.View.OnFailure(err)
	} else {
		uid := uuid.NewV4()
		password, err := rp.PasswordHandler.CreateHash(Details.Password)
		if err != nil {
			rp.View.OnFailure(err)
		} else {
			user := Details.ToUser(uid, password)
			err = rp.UserRepository.Add(&user, []string{"SUPER_ADMINISTRATOR"})
			if err != nil {
				rp.View.OnFailure(err)
			} else {
				rp.View.OnSuccess()
			}
		}
	}
}

type RegistrationView struct {
	form            *tview.Form
	ViewHandler     base.ViewHandler
	UserRepository  users.UserRepository
	PasswordHandler security.PasswordHandler
	presenter       Actions
}

func NewRegistrationView(
	ViewHandler base.ViewHandler,
	UserRepository users.UserRepository,
	PasswordHandler security.PasswordHandler) *RegistrationView {
	view := &RegistrationView{
		ViewHandler:     ViewHandler,
		UserRepository:  UserRepository,
		PasswordHandler: PasswordHandler,
	}
	view.init()
	return view
}

func (rv *RegistrationView) init() {
	registrationDetails := &RegistrationDetails{}
	rv.presenter = &RegistrationPresenter{
		View:            rv,
		UserRepository:  rv.UserRepository,
		PasswordHandler: rv.PasswordHandler,
	}
	rv.form = tview.NewForm().
		AddInputField("First name", "", 20, nil, func(FirstName string) {
			registrationDetails.FirstName = FirstName
		}).
		AddInputField("Last name", "", 20, nil, func(LastName string) {
			registrationDetails.LastName = LastName
		}).
		AddInputField("Email", "", 30, nil, func(EmailAddress string) {
			registrationDetails.EmailAddress = EmailAddress
		}).
		AddInputField("Phone Number (+CODE)", "", 20, nil, func(PhoneNumber string) {
			registrationDetails.PhoneNumber = PhoneNumber
		}).
		AddInputField("User Name", "", 20, nil, func(UserName string) {
			registrationDetails.Username = UserName
		}).
		AddPasswordField("Password", "", 15, '*', func(Password string) {
			registrationDetails.Password = Password
		}).
		AddPasswordField("Confirm Password", "", 15, '*', func(ConfirmPassword string) {
			registrationDetails.ConfirmPassword = ConfirmPassword
		}).
		AddButton("Register", func() {
			rv.presenter.RegisterAdministrator(registrationDetails)
		}).
		AddButton("Back", func() {
			rv.ViewHandler.To("LOGIN")
		})
	rv.form.SetBorder(true).SetTitle("REGISTRATION").SetTitleAlign(tview.AlignCenter)
	rv.ViewHandler.Register("REGISTRATION", rv.form, false)
}

func (rv *RegistrationView) OnSuccess() {
	rv.ViewHandler.To("LOGIN")
}

func (rv *RegistrationView) OnFailure(Error error) {
	rv.ViewHandler.PopUpError("REGISTRATION", Error.Error())
}

func (rv *RegistrationView) OnBack() {
	rv.ViewHandler.To("LOGIN")
}
