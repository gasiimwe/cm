package base

import (
	"errors"

	"github.com/rivo/tview"
)

type ViewHandler interface {
	To(ViewID string) error
	Register(Name string, View tview.Primitive, Show bool)
	Init() error
	End()
	PopUpError(ParentPage, Message string)
}

type DisplayView struct {
	Name      string
	Primitive tview.Primitive
	Show      bool
}

type ViewHandlerImpl struct {
	app   *tview.Application
	pages *tview.Pages
	views []*DisplayView
}

func NewViewHandler() *ViewHandlerImpl {
	return &ViewHandlerImpl{}
}

func (vhi *ViewHandlerImpl) Init() error {
	vhi.app = tview.NewApplication()
	vhi.pages = tview.NewPages()
	for _, view := range vhi.views {
		vhi.pages.AddPage(view.Name, view.Primitive, true, view.Show)
	}
	return vhi.app.SetRoot(vhi.pages, true).SetFocus(vhi.pages).Run()
}

func (vhi *ViewHandlerImpl) End() {
	vhi.app.Stop()
}

func (vhi *ViewHandlerImpl) Register(Name string, View tview.Primitive, Show bool) {
	vhi.views = append(vhi.views, &DisplayView{Name, View, Show})
}

func (vhi *ViewHandlerImpl) To(ViewID string) error {
	if page := vhi.pages.SwitchToPage(ViewID); page == nil {
		return errors.New("Unknown Page ")
	}
	return nil
}

func (vhi *ViewHandlerImpl) PopUpError(Name, Message string) {
	modal := tview.NewModal().
		SetText(Message).
		AddButtons([]string{"OK"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonLabel == "OK" {
				vhi.pages.RemovePage("POP_UP")
				vhi.pages.SwitchToPage(Name)
			}
		})
	vhi.pages.AddAndSwitchToPage("POP_UP", modal, false)
}
