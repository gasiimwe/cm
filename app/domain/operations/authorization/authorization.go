package authorization

import (
	"errors"
	"github.com/rivo/tview"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/domain/operations/base"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
)

type View interface {
	OnSuccess(Administrator *users.User)
	OnFailure(Error error)
	OnExit()
}

type Actions interface {
	Login(Username, Password string)
}

type AuthorizationPresenter struct {
	View            View
	UserRepository  users.UserRepository
	PasswordHandler security.PasswordHandler
}

func (ap *AuthorizationPresenter) Login(Username, Password string) {
	user, err := ap.UserRepository.FetchUserByUsername(Username)
	if err != nil {
		ap.View.OnFailure(err)
	} else {
		if ok, err := ap.PasswordHandler.CompareHashToPassword(Password, user.Password); err != nil {
			ap.View.OnFailure(err)
		} else if ok {
			ap.View.OnSuccess(user)
		} else {
			ap.View.OnFailure(errors.New("Credentials are not a match"))
		}
	}
}

type AuthorizationView struct {
	form            *tview.Form
	ViewHandler     base.ViewHandler
	UserRepository  users.UserRepository
	PasswordHandler security.PasswordHandler
	presenter       Actions
	username        string
	password        string
}

func NewAuthorizationView(
	ViewHandler base.ViewHandler,
	UserRepository users.UserRepository,
	PasswordHandler security.PasswordHandler) *AuthorizationView {
	view := &AuthorizationView{
		ViewHandler:     ViewHandler,
		UserRepository:  UserRepository,
		PasswordHandler: PasswordHandler,
	}
	view.init()
	return view
}

func (a *AuthorizationView) init() {
	a.presenter = &AuthorizationPresenter{
		UserRepository:  a.UserRepository,
		PasswordHandler: a.PasswordHandler,
		View:            a,
	}
	a.form = tview.NewForm().
		AddInputField("User name", "", 15, nil, func(UserName string) {
			a.username = UserName
		}).
		AddPasswordField("Password", "", 15, '*', func(Password string) {
			a.password = Password
		}).
		AddButton("Login", func() {
			a.presenter.Login(a.username, a.password)
		}).
		AddButton("Register", func() {
			if err := a.ViewHandler.To("REGISTRATION"); err != nil {
				logging.GetInstance().Fatalf("Failed to switch view: %v ", err)
			}
		}).
		AddButton("Quit", func() {
			a.OnExit()
		})
	a.form.SetBorder(true).SetTitle("LOGIN").SetTitleAlign(tview.AlignLeft)
	a.ViewHandler.Register("LOGIN", a.form, true)
}

func (a *AuthorizationView) OnSuccess(Administrator *users.User) {
	a.ViewHandler.To("ADMINISTRATION")
}

func (a *AuthorizationView) OnFailure(Error error) {
	a.ViewHandler.PopUpError("LOGIN", Error.Error())
}

func (a *AuthorizationView) OnExit() {
	a.ViewHandler.End()
}
