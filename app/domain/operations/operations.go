package operations

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/domain/operations/administration"
	"gitlab.com/gasiimwe/cm/app/domain/operations/authorization"
	"gitlab.com/gasiimwe/cm/app/domain/operations/base"
	"gitlab.com/gasiimwe/cm/app/domain/operations/registration"
	"gitlab.com/gasiimwe/cm/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gitlab.com/gasiimwe/cm/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server"
)

type ServiceOperations struct {
	rolesRepository       roles.RoleRepository
	permissionsRepository permissions.PermissionRepository
	usersRepository       users.UserRepository
	discoveryService      discovery.Service
	configs               configuration.Configuration
	details               server.Details
	dataSource            database.DataSource
	passwordHandler       security.PasswordHandler
}

func NewServiceOperations() *ServiceOperations {
	instance := &ServiceOperations{}
	instance.initialize()
	return instance
}

func (so *ServiceOperations) createDataSource() database.DataSource {
	return database.
		NewBuilder().
		EnableLogging().
		MaxConnectionLifeTime(time.Hour).
		MetricRegistry(prometheus.NewRegistry()).
		MaxIdleConnections(so.configs.DatabaseParameters.MaxIdleConnections).
		MaxOpenConnections(so.configs.DatabaseParameters.MaxOpenConnections).
		Uri(so.configs.DatabaseParameters.URI).
		Build()
}

func (so *ServiceOperations) initialize() {
	so.details = server.FetchApplicationDetails()
	if configuration.IsConsulEnabled() {
		so.initializeServiceDiscovery()
		so.configs = configuration.ParseFromConsul(so.discoveryService, so.details.Name)
	} else {
		so.configs = configuration.ParseFromResource()
	}
	so.dataSource = so.createDataSource()
	so.setUpPasswordHandler()
	so.setUpRepositories()
}

func (so *ServiceOperations) setUpPasswordHandler() {
	so.passwordHandler = &security.Argon2PasswordHandler{
		EncryptionParameters: so.configs.EncryptionParameters,
	}
}

func (so *ServiceOperations) setUpRepositories() {
	so.permissionsRepository = permissions.New(so.dataSource)
	so.rolesRepository = roles.New(so.dataSource)
	so.usersRepository = users.New(so.dataSource)
}

func (so *ServiceOperations) Administration() error {
	defer so.cleanUp()
	viewHandler := base.NewViewHandler()
	authorization.NewAuthorizationView(
		viewHandler,
		so.usersRepository,
		so.passwordHandler,
	)
	registration.NewRegistrationView(
		viewHandler,
		so.usersRepository,
		so.passwordHandler,
	)
	administration.NewAdministrationView(
		viewHandler,
	)
	if err := viewHandler.Init(); err != nil {
		return err
	}
	return nil
}

func (so *ServiceOperations) Setup() error {
	defer so.cleanUp()
	log := logging.GetInstance()
	roleValues := make([]RoleValue, 0)
	extractedPermissions := make(map[string]permissions.Permission, 0)
	permissionsList := make([]permissions.Permission, 0)
	extractedRoles := make(map[string]*roles.Role, 0)

	data, err := server.Asset("resources/acm_data.json")
	if err != nil {
		return err
	}
	if err = json.Unmarshal(data, &roleValues); err != nil {
		return err
	}

	for _, roleValue := range roleValues {
		p := make([]permissions.Permission, 0)
		log.Infof("ROLE: %s", roleValue.Name)
		for _, value := range roleValue.Permissions {
			log.Infof("PERMISSION : %s", value)
			p = append(p, permissions.Permission{Name: value})
			extractedPermissions[value] = permissions.Permission{Name: value}
		}
		extractedRoles[roleValue.Name] = &roles.Role{
			Name:        roleValue.Name,
			Permissions: p,
		}
	}
	for _, v := range extractedPermissions {
		permissionsList = append(permissionsList, v)
	}
	if err := so.permissionsRepository.Add(permissionsList); err != nil {
		return err
	}
	log.Println("PERSISTED PERMISSIONS")
	for _, v := range extractedRoles {
		if err := so.rolesRepository.Add(v); err != nil {
			return err
		}
	}
	return nil
}

func (so *ServiceOperations) initializeServiceDiscovery() {
	address := fmt.Sprintf("%s:%s", os.Getenv("CONSUL_HOST"), os.Getenv("CONSUL_PORT"))
	datacenter := os.Getenv("CONSUL_DATACENTER")
	token := os.Getenv("CONSUL_TOKEN")
	so.discoveryService = discovery.New(address, datacenter, token)
}

func (so *ServiceOperations) cleanUp() {
	so.dataSource.Close()
}
