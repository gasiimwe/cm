package administration

import (
	"github.com/rivo/tview"
	"gitlab.com/gasiimwe/cm/app/domain/operations/base"
)

type Actions interface {
	ToRoles()
	ToPermissions()
	ToDetailsUpdate()
}

type AdministrationView struct {
	ViewHandler      base.ViewHandler
	dropdown         *tview.DropDown
	roleBtn          *tview.Button
	permissionBtn    *tview.Button
	detailsUpdateBtn *tview.Button
	exitButton       *tview.Button
}

func NewAdministrationView(ViewHandler base.ViewHandler) *AdministrationView {
	view := &AdministrationView{ViewHandler: ViewHandler}
	view.init()
	return view
}

func (a *AdministrationView) init() {
	a.dropdown = tview.NewDropDown().
		SetLabel("Select an option (hit Enter): ").
		SetOptions([]string{
			"Permissions",
			"Roles",
			"Admin Details", "Exit"}, func(Text string, Index int) {
			switch Text {
			case "Permissions":
				a.ToPermissions()
			case "Roles":
				a.ToRoles()
			case "Admin Details":
				a.ToDetailsUpdate()
			case "Exit":
				a.ViewHandler.End()
			default:
			}
		})

	a.ViewHandler.Register("ADMINISTRATION", a.dropdown, false)
}

func (a *AdministrationView) ToRoles() {
	// a.ViewHandler.To("ROLES")
	a.ViewHandler.PopUpError("ADMINISTRATION", "Not implemented yet")
}

func (a *AdministrationView) ToPermissions() {
	// a.ViewHandler.To("PERMISSIONS")
	a.ViewHandler.PopUpError("ADMINISTRATION", "Not implemented yet")
}

func (a *AdministrationView) ToDetailsUpdate() {
	// a.ViewHandler.To("DETAILS_UPDATE")
	a.ViewHandler.PopUpError("ADMINISTRATION", "Not implemented yet")
}
