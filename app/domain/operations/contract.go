package operations

type RoleValue struct {
	Name        string   `json:"name"`
	Permissions []string `json:"permissions"`
}
