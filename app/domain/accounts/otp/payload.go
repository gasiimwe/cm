package otp

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type OTPEntryDetail struct {
	UID     uuid.UUID `sql:"column:uid;type:uuid"`
	DueDate time.Time `sql:"column:due_date;type:timestamp; not null"`
}
