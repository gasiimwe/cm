package otp

import (
	"time"

	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
)

type SqlOTPRepository struct {
	dataSource database.DataSource
}

func New(dataSource database.DataSource) OTPRepository {
	return &SqlOTPRepository{dataSource: dataSource}
}

func (sor *SqlOTPRepository) Add(reference, value, uid string, dueTime *time.Time) error {
	var sql = "INSERT INTO otp_entry_tb(user_id, reference, value, due_date) " +
		"VALUES((SELECT id FROM user_tb WHERE uid = ?), ?, ?, ?)"
	tx := sor.dataSource.Connection().Begin()
	if err := tx.Exec(sql, uid, reference, value, dueTime).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (sor *SqlOTPRepository) Fetch(reference, value string) (*OTPEntryDetail, error) {
	entry := &OTPEntryDetail{}
	connection := sor.dataSource.Connection()
	if err := connection.Raw(
		"SELECT u.uid, o.due_date FROM user_tb AS u "+
			"INNER JOIN otp_entry_tb AS o ON u.id = o.user_id "+
			"WHERE o.reference = ? AND o.value = ?", reference, value).First(entry).Error; err != nil {
		return entry, err
	}
	return entry, nil
}

func (sor *SqlOTPRepository) RemoveExpired(reference, value string) error {
	var sql = "DELETE FROM user_tb " +
		"USING otp_entry_tb " +
		"WHERE user_tb.id = otp_entry_tb.user_id " +
		"AND otp_entry_tb.reference = ? " +
		"AND otp_entry_tb.value = ?"
	tx := sor.dataSource.Connection().Begin()
	if err := tx.Exec(sql, reference, value).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (sor *SqlOTPRepository) Remove(OlderThan *time.Time) error {
	var sql = "DELETE FROM otp_entry_tb WHERE due_date < ?"
	tx := sor.dataSource.Connection().Begin()
	if err := tx.Exec(sql, OlderThan).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
