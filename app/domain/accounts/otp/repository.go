package otp

import (
	"time"
)

type OTPRepository interface {
	Add(reference, value, uid string, dueTime *time.Time) error
	Fetch(reference, value string) (*OTPEntryDetail, error)
	RemoveExpired(reference, value string) error
	Remove(OlderThan *time.Time) error
}
