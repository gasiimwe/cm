package otp

import (
	"errors"
	"math/rand"
	"time"

	"github.com/jasonlvhit/gocron"
	uuid "github.com/satori/go.uuid"
)

const (
	OTP_LENGTH                   = 6
	PASSED_VALIDITY_DATE         = "Passed Validity Period"
	OTP_HOURLY_CLEAN_UP_DURATION = 2
)

type OTPHandler struct {
	scheduler  *gocron.Scheduler
	repository OTPRepository
	duration   time.Duration
}

func NewHandler(repository OTPRepository, duration time.Duration) *OTPHandler {
	instance := &OTPHandler{
		repository: repository,
		duration:   duration,
	}
	instance.init()
	return instance
}

func (oh *OTPHandler) init() {
	oh.scheduler = gocron.NewScheduler()
	go func() {
		oh.scheduler.Every(OTP_HOURLY_CLEAN_UP_DURATION).Hours().Do(oh.removeOldEntries)
		<-oh.scheduler.Start()
	}()
}

func (oh *OTPHandler) Create(uid string) (string, string, error) {
	otp := oh.randomNumber(OTP_LENGTH)
	reference := uuid.NewV4().String()
	location, _ := time.LoadLocation("UTC")
	dueDate := time.Now().Add(oh.duration).In(location)
	if err := oh.repository.Add(reference, otp, uid, &dueDate); err != nil {
		return "", "", err
	}
	return reference, otp, nil
}

func (oh *OTPHandler) FetchMatch(reference, value string) (string, error) {
	details, err := oh.repository.Fetch(reference, value)
	if err != nil {
		return "", err
	}
	location, _ := time.LoadLocation("UTC")
	if details.DueDate.Before(time.Now().In(location)) {
		oh.repository.RemoveExpired(reference, value)
		return "", errors.New(PASSED_VALIDITY_DATE)
	}
	return details.UID.String(), nil
}

func (oh *OTPHandler) randomNumber(length int) string {
	var numbers = []rune("0123456789")
	b := make([]rune, length)
	for i := range b {
		b[i] = numbers[rand.Intn(len(numbers))]
	}
	return string(b)
}

func (oh *OTPHandler) GetDuration() time.Duration {
	return oh.duration
}

func (oh *OTPHandler) removeOldEntries() {
	currentTime := time.Now()
	oh.repository.Remove(&currentTime)
}
