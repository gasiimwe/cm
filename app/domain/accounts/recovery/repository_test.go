package recovery_test

import (
	"database/sql"
	"fmt"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/recovery"
	"gitlab.com/gasiimwe/cm/app/domain/acm/permissions"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestRecoveryRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Recovery Repository Suite")
}

type testDataSource struct {
	db *gorm.DB
}

type RecoveryRequest struct {
	ID        int64     `gorm:"AUTO_INCREMENT;column:id;primary_key" json:"-"`
	UserId    int64     `gorm:"column:user_id;not null"`
	DueDate   time.Time `gorm:"column:due_date;type:timestamp; not null"`
	Method    string    `gorm:"column:method;type:varchar; not null"`
	Reference uuid.UUID `gorm:"column:reference;type:uuid; not null"`
}

func (RecoveryRequest) TableName() string {
	return "recovery_tb"
}

func (tds *testDataSource) Connection() *gorm.DB {
	if tds.db == nil {
		var err error
		tds.db, err = gorm.Open(
			sqlite.Open("file::memory:?mode=memory&cache=shared"),
			&gorm.Config{})
		if err != nil {
			panic(err)
		}
	}
	return tds.db
}

func (tds *testDataSource) Close() error {
	if tds.db != nil {
		return tds.Close()
	}
	return nil
}

func dataSourceProvider() database.DataSource {
	return &testDataSource{}
}

func migrate(dataSource database.DataSource) {
	rule0 := `CREATE TRIGGER IF NOT EXISTS update_permission_idx AFTER INSERT ON permission_tb
		   BEGIN
			UPDATE permission_tb SET idx=id WHERE id=NEW.id;
		   END;`

	rule1 := `CREATE TRIGGER update_role_idx AFTER INSERT ON role_tb
			  BEGIN
			   UPDATE role_tb SET idx=id WHERE id=NEW.id;
			  END;`
	rule2 := `CREATE TRIGGER update_user_idx AFTER INSERT ON user_tb
			  BEGIN
			   UPDATE user_tb SET idx=id WHERE id=NEW.id;
			  END;`

	dataSource.Connection().AutoMigrate(
		&permissions.Permission{},
		&roles.Role{},
		&users.User{},
		&RecoveryRequest{})
	dataSource.Connection().Exec(rule0)
	dataSource.Connection().Exec(rule1)
	dataSource.Connection().Exec(rule2)
}

func addRole(dataSource database.DataSource, RoleName string, addedPermissions []string) {
	added := make([]permissions.Permission, 0)
	for _, permission := range addedPermissions {
		added = append(added, permissions.Permission{Name: permission})
	}
	permissions.New(dataSource).Add(added)
	roles.New(dataSource).Add(&roles.Role{Name: RoleName, Permissions: added})
}

var _ = Describe("Recovery repository tests", func() {
	dataSource := dataSourceProvider()
	migrate(dataSource)
	recoveryRepository := recovery.New(dataSource)
	var phoneNumber = "+256794222334"
	var emailAddress = "aron.kayongo@gmail.com"
	var uid = uuid.NewV4()
	Context("Given that a known user exists", func() {
		BeforeEach(func() {
			addRole(dataSource, "CUSTOMER", []string{"CAN_CREATE_CART", "CAN_ADD_TO_CART"})
			users.New(dataSource).Add(&users.User{
				FirstName: "Aron",
				LastName:  "Kayongo",
				Gender:    "MALE",
				Email:     emailAddress,
				Phone:     phoneNumber,
				UserName:  "aron",
				Password:  "kayongo",
				Activated: true,
				UID:       uid,
			}, []string{"CUSTOMER"})
		})

		AfterEach(func() {
			dataSource.Connection().Exec("DELETE FROM user_tb")
			dataSource.Connection().Exec("DELETE FROM role_tb")
		})

		It("Then you can recover user by mobile phone", func() {
			reference := uuid.NewV4()
			err := recoveryRepository.Add(&recovery.RecoveryOrder{
				UID:       uid,
				DueDate:   time.Now().Add(24 * time.Hour),
				Method:    recovery.PHONE_METHOD,
				Reference: reference,
			})
			Expect(err).NotTo(HaveOccurred())

		})
	})

})
