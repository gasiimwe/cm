package recovery

import (
	"errors"
	"time"

	"gitlab.com/gasiimwe/cm/app/infrastructure/database"
)

type SqlRecoveryRepository struct {
	dataSource database.DataSource
}

func New(dataSource database.DataSource) RecoveryRepository {
	return &SqlRecoveryRepository{dataSource: dataSource}
}

func (srr *SqlRecoveryRepository) Add(Order *RecoveryOrder) error {

	tx := srr.dataSource.Connection().Begin()
	var sql = "INSERT INTO recovery_tb(user_id,due_date,method,reference) " +
		"VALUES((SELECT id FROM user_tb WHERE uid=?),?,?,?)"
	if err := tx.Raw(sql, Order.UID, Order.DueDate, Order.Method, Order.Reference).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (srr *SqlRecoveryRepository) Fetch(Reference string) (*RecoveryDetail, error) {
	recoveryDetail := &RecoveryDetail{}
	connection := srr.dataSource.Connection()
	location, _ := time.LoadLocation("UTC")

	if err := connection.Raw(`SELECT u.uid, r.due_date, r.method 
				   FROM recovery_tb AS r INNER JOIN user_tb AS u ON r.user_id=u.id 
				   WHERE r.reference = ?`, Reference).Scan(recoveryDetail).Error; err != nil {

		return nil, errors.New(NO_RECOVERY_REQUEST)
	} else if recoveryDetail.DueDate.Before(time.Now().In(location)) {
		return nil, errors.New(DUE_TIME_PAST)
	}
	return recoveryDetail, nil
}
