package recovery

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type RecoveryOrder struct {
	UID       string
	DueDate   time.Time
	Method    string
	Reference string
}

type RecoveryDetail struct {
	UID     uuid.UUID `sql:"column:uid;type:uuid"`
	DueDate time.Time `sql:"column:due_date;type:timestamp; not null"`
	Method  string    `sql:"column:method;type:varchar; not null"`
}
