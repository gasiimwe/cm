package recovery

const (
	NO_RECOVERY_REQUEST = "No recovery request found"
	DUE_TIME_PAST       = "Due time for recovery passed"
	EMAIL_METHOD        = "EMAIL"
	PHONE_METHOD        = "PHONE"
)

type RecoveryRepository interface {
	Add(Order *RecoveryOrder) error
	Fetch(Reference string) (*RecoveryDetail, error)
}
