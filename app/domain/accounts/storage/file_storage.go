package storage

import (
	"mime/multipart"
	"net/http"
)

// FileStorage :
type FileStorage interface {
	Save(Name string, TargetFile multipart.File) (string, error)
	Fetch(Referance string, w http.ResponseWriter)
}
