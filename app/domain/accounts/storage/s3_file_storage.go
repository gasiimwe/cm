package storage

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	uuid "github.com/satori/go.uuid"
)

type S3FileStorage struct {
	configuration aws.Config
	bucket        string
}

func NewS3FileStorage(AccessKey, SecretKey, Host, Bucket, Region string) FileStorage {
	s3s := &S3FileStorage{bucket: Bucket}
	s3s.prepare(AccessKey, SecretKey, Host, Region)
	return s3s
}

func (s3f *S3FileStorage) prepare(AccessKey, SecretKey, Host, Region string) {
	s3f.configuration = aws.Config{
		Credentials:      credentials.NewStaticCredentials(AccessKey, SecretKey, ""),
		Endpoint:         aws.String(Host),
		Region:           aws.String(Region),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
	}
}

func (s3f *S3FileStorage) Save(Name string, TargetFile multipart.File) (string, error) {
	var extension = filepath.Ext(Name)
	var ref = uuid.NewV4().String()
	newSession := session.New(&s3f.configuration)
	uploader := s3manager.NewUploader(newSession)
	filename := fmt.Sprintf("%s%s", ref, extension)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s3f.bucket), // Bucket to be used
		Key:    aws.String(filename),   // Name of the file to be saved
		Body:   TargetFile,             // File
	})
	if err != nil {
		return "", err
	}
	return filename, nil
}

func (s3f *S3FileStorage) Fetch(Referance string, w http.ResponseWriter) {
	buffer := &aws.WriteAtBuffer{}
	newSession := session.New(&s3f.configuration)
	downloader := s3manager.NewDownloader(newSession)
	numberOfBytes, err := downloader.Download(buffer, &s3.GetObjectInput{
		Bucket: aws.String(s3f.bucket),
		Key:    aws.String(Referance),
	})
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	data := buffer.Bytes()
	FileHeader := make([]byte, 512)
	//Copy the headers into the FileHeader buffer
	copy(FileHeader[:], data[0:512])
	//Get content type of file
	FileContentType := http.DetectContentType(FileHeader)
	//Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename="+Referance)
	w.Header().Set("Content-Type", FileContentType)
	w.Header().Set("Content-Length", string(numberOfBytes))
	w.WriteHeader(http.StatusOK)
}
