package messenger

import (
	"time"
)

const (
	OTP_REGISTRATION  = "otp-registration"
	PASSWORD_RECOVERY = "password-recovery"
	ACCOUNT_DETAILS   = "account-details"
)

type OTPConfirmationOrder struct {
	FirstName string
	LastName  string
	Email     string
	OTPValue  string
	Reference string
	Expiry    time.Duration
}

type PasswordResetOrder struct {
	FirstName string
	LastName  string
	Email     string
	Reference string
	Expiry    time.Duration
}

type AccountDetailsOrder struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
}

type AccountDetailsContent struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

type OTPContent struct {
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	OTPValue  string        `json:"otp"`
	Reference string        `json:"reference"`
	Expiry    time.Duration `json:"expiry"`
	Link      string        `json:"link"`
}

type PasswordResetContent struct {
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	Reference string        `json:"reference"`
	Link      string        `json:"link"`
	Expiry    time.Duration `json:"expiry"`
}
