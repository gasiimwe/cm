package messenger

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
	nats "github.com/nats-io/nats.go"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/messenger/email"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
)

const (
	REGISTRATION_LINK         = "registration-confirmation"
	PASSWORD_RESET_LINK       = "password-reset"
	TOPIC                     = "v1/email-service/single-email-order"
	TIMEOUT_SECONDS           = 10
	REGISTRATION_SUBJECT      = "MTWA CITES Registration Confirmation"
	PASSWORD_RECOVERY_SUBJECT = "MTWA CITES Password Recovery"
	ACCOUNT_DETAILS_SUBJECT   = "MTWA CITES Account Details"
)

type natsBasedClient struct {
	connection *nats.Conn
	servers    []string
	links      map[string]string
}

func New(servers []string, links map[string]string) Client {
	return &natsBasedClient{
		servers: servers,
		links:   links,
	}
}

func (dbc *natsBasedClient) StartUp() error {
	return dbc.initialize()
}

func (dbc *natsBasedClient) initialize() error {
	var err error
	LOG := logging.GetInstance()
	dbc.connection, err = nats.Connect(strings.Join(dbc.servers, ","),
		nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
			LOG.Errorf("Got disconnected! Reason: %q\n", err)
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			LOG.Infof("Got reconnected to %v!\n", nc.ConnectedUrl())
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			LOG.Errorf("Connection closed. Reason: %q\n", nc.LastError())
		}),
	)
	if err != nil {
		LOG.Error(err)
		return err
	} else if dbc.connection.IsConnected() {
		LOG.Infof("CONNECTED TO NATS.IO ")
	}
	return nil
}

func (dbc *natsBasedClient) SendOTPOrder(order *OTPConfirmationOrder) error {

	content := &OTPContent{
		FirstName: order.FirstName,
		LastName:  order.LastName,
		OTPValue:  order.OTPValue,
		Reference: order.Reference,
		Expiry:    order.Expiry / time.Hour,
		Link:      dbc.links[REGISTRATION_LINK],
	}

	body, err := json.Marshal(content)
	if err != nil {
		return err
	}

	request := &email.EmailRequest{
		To:      order.Email,
		Type:    OTP_REGISTRATION,
		Subject: REGISTRATION_SUBJECT,
		Order:   body,
	}

	response, err := dbc.send(request)
	if err != nil {
		return err
	}

	if response.Status == email.EmailResponse_FAILED {
		return fmt.Errorf(response.GetMessage())
	}

	logging.GetInstance().Infof("EMAIL : %s", response.GetMessage())
	return nil
}
func (dbc *natsBasedClient) SendPasswordResetOrder(order *PasswordResetOrder) error {
	content := &PasswordResetContent{
		FirstName: order.FirstName,
		LastName:  order.LastName,
		Reference: order.Reference,
		Expiry:    order.Expiry / time.Hour,
		Link:      dbc.links[PASSWORD_RESET_LINK],
	}
	body, err := json.Marshal(content)
	if err != nil {
		return err
	}
	request := &email.EmailRequest{
		To:      order.Email,
		Type:    PASSWORD_RECOVERY,
		Subject: PASSWORD_RECOVERY_SUBJECT,
		Order:   body,
	}

	response, err := dbc.send(request)
	if err != nil {
		return err
	}

	if response.Status == email.EmailResponse_FAILED {
		return fmt.Errorf(response.GetMessage())
	}

	logging.GetInstance().Infof("EMAIL : %s", response.GetMessage())
	return nil
}

func (dbc *natsBasedClient) SendAccountDetailsOrder(order *AccountDetailsOrder) error {
	content := &AccountDetailsContent{
		FirstName: order.FirstName,
		LastName:  order.LastName,
		Username:  order.Email,
		Password:  order.Password,
	}

	body, err := json.Marshal(content)
	if err != nil {
		return err
	}

	request := &email.EmailRequest{
		To:      order.Email,
		Type:    ACCOUNT_DETAILS,
		Subject: ACCOUNT_DETAILS_SUBJECT,
		Order:   body,
	}

	response, err := dbc.send(request)
	if err != nil {
		return err
	}

	if response.Status == email.EmailResponse_FAILED {
		return fmt.Errorf(response.GetMessage())
	}

	logging.GetInstance().Infof("EMAIL : %s", response.GetMessage())
	return nil
}

func (dbc *natsBasedClient) send(request *email.EmailRequest) (*email.EmailResponse, error) {
	data, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}

	message, err := dbc.connection.Request(TOPIC, data, TIMEOUT_SECONDS*time.Second)
	if err != nil {
		return nil, err
	}
	response := &email.EmailResponse{}
	if err := proto.Unmarshal(message.Data, response); err != nil {
		return nil, err
	}
	return response, nil
}

func (dbc *natsBasedClient) Shutdown() {
	if dbc.connection.IsConnected() {
		dbc.connection.Close()
	}
}
