package messenger

type Client interface {
	StartUp() error
	SendOTPOrder(order *OTPConfirmationOrder) error
	SendPasswordResetOrder(order *PasswordResetOrder) error
	SendAccountDetailsOrder(order *AccountDetailsOrder) error
	Shutdown()
}
