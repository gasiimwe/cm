package staff

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/gasiimwe/cm/app/domain/accounts/storage"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/account-service/accounts/staff"

type StaffController struct {
	*web.ControllerBase
	accountService *AccountService
	fileStorage    storage.FileStorage
}

func NewController(accountService *AccountService, fileStorage storage.FileStorage) web.HTTPController {
	return &StaffController{
		accountService: accountService,
		fileStorage:    fileStorage,
	}
}

func (sc *StaffController) Name() string {
	return "staff-controller"
}

func (sc *StaffController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"POST",
		sc.register,
	)
	RouteRegistry.AddRestricted(
		fmt.Sprintf("%s/{staff-identifier:%s}", BASE_PATH, web.GUID_PATTERN),
		[]string{"SUPER_ADMINISTRATOR"},
		"PUT",
		sc.update,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		sc.search,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH+"/roles",
		[]string{"SUPER_ADMINISTRATOR"},
		"GET",
		sc.roles,
	)
	RouteRegistry.AddRestricted(
		fmt.Sprintf("%s/{staff-identifier:%s}", BASE_PATH, web.GUID_PATTERN),
		[]string{"SUPER_ADMINISTRATOR"},
		"DELETE",
		sc.remove,
	)
	RouteRegistry.Add(
		BASE_PATH+"/images/{image-identifier}",
		false,
		"GET",
		sc.photo,
	)
	RouteRegistry.Add(
		BASE_PATH+"/recover-password",
		false,
		"PUT",
		sc.recoverPassword,
	)
	RouteRegistry.Add(
		BASE_PATH+"/reset-password",
		false,
		"PUT",
		sc.resetPassword,
	)
}

func (sc *StaffController) register(w http.ResponseWriter, r *http.Request) {
	var photoReference string
	form, err := ParseForm(r)
	if err != nil {
		sc.BadRequest(w, err)
		return
	} else if err := form.Validate(); err != nil {
		sc.BadRequest(w, err)
		return
	} else if form.HasPhoto() {
		defer form.Photo.Close()
		if photoReference, err = sc.fileStorage.Save(form.PhotoHeader.Filename, form.Photo); err != nil {
			sc.ServiceFailure(w, err)
		}
	}
	details := form.ToDetails()
	if err := sc.accountService.Register(
		details,
		photoReference,
	); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OKNoResponse(w)
	}
}

func (sc *StaffController) update(w http.ResponseWriter, r *http.Request) {
	var photoReference string
	form, err := ParseForm(r)
	if err != nil {
		sc.BadRequest(w, err)
	} else if form.HasPhoto() {
		defer form.Photo.Close()
		if photoReference, err = sc.fileStorage.Save(form.PhotoHeader.Filename, form.Photo); err != nil {
			sc.ServiceFailure(w, err)
		}
	}
	details := form.ToDetails()
	staffIdentifier := sc.GetVar("staff-identifier", r)
	if err := sc.accountService.UpdateDetails(staffIdentifier, details, photoReference); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OKNoResponse(w)
	}
}

func (sc *StaffController) search(w http.ResponseWriter, r *http.Request) {
	query := sc.GetQueryString("q", r)
	limit := sc.GetQueryInt("limit", r)
	offset := sc.GetQueryInt("offset", r)

	if staffs, err := sc.accountService.Search(
		query,
		limit,
		offset,
	); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OK(w, web.Paged{Data: staffs, Total: len(staffs)})
	}
}

func (sc *StaffController) roles(w http.ResponseWriter, r *http.Request) {
	if roles, err := sc.accountService.Roles(); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OK(w, web.Paged{Data: roles, Total: len(roles)})
	}
}

func (sc *StaffController) remove(w http.ResponseWriter, r *http.Request) {
	staffIdentifier := sc.GetVar("staff-identifier", r)
	if err := sc.accountService.DeRegister(staffIdentifier); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OKNoResponse(w)
	}
}

func (sc *StaffController) photo(w http.ResponseWriter, r *http.Request) {
	imageIdentifier := sc.GetVar("image-identifier", r)
	sc.fileStorage.Fetch(imageIdentifier, w)
}

func (sc *StaffController) recoverPassword(w http.ResponseWriter, r *http.Request) {
	resetRequest := &PasswordResetRequest{}
	if err := json.NewDecoder(r.Body).Decode(resetRequest); err != nil {
		sc.BadRequest(w, err)
	} else if err = sc.accountService.RequestPasswordReset(resetRequest); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OKNoResponse(w)
	}
}

func (sc *StaffController) resetPassword(w http.ResponseWriter, r *http.Request) {
	resetOrder := &PasswordResetOrder{}
	if err := json.NewDecoder(r.Body).Decode(resetOrder); err != nil {
		sc.BadRequest(w, err)
	} else if err = sc.accountService.ResetPassword(resetOrder); err != nil {
		sc.ServiceFailure(w, err)
	} else {
		sc.OKNoResponse(w)
	}
}
