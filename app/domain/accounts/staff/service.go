package staff

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/messenger"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/recovery"
	"gitlab.com/gasiimwe/cm/app/domain/acm/roles"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
)

const PASSWORD_LENGTH = 6

type AccountService struct {
	userRepository     users.UserRepository
	rolesRepository    roles.RoleRepository
	recoveryRepository recovery.RecoveryRepository
	passwordHandler    security.PasswordHandler
	messengerClient    messenger.Client
}

func New(
	UserRepository users.UserRepository,
	RolesRepository roles.RoleRepository,
	RecoveryRepository recovery.RecoveryRepository,
	PasswordHandler security.PasswordHandler,
	MessengerClient messenger.Client) *AccountService {
	return &AccountService{
		userRepository:     UserRepository,
		rolesRepository:    RolesRepository,
		recoveryRepository: RecoveryRepository,
		passwordHandler:    PasswordHandler,
		messengerClient:    MessengerClient,
	}
}

func (as *AccountService) Register(details *StaffMemberDetails, photoId string) error {
	result, err := password.Generate(PASSWORD_LENGTH, 4, 2, false, false)
	if err != nil {
		return err
	}
	user := as.createUser(details, photoId)
	hash, err := as.passwordHandler.CreateHash(result)
	if err != nil {
		return err
	}
	user.Password = hash
	order := &messenger.AccountDetailsOrder{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  result,
	}
	if err = as.messengerClient.SendAccountDetailsOrder(order); err != nil {
		return err
	}
	return as.userRepository.Add(user, details.Roles)
}

func (as *AccountService) createUser(details *StaffMemberDetails, photoId string) *users.User {
	return &users.User{
		UID:          uuid.NewV4(),
		FirstName:    details.FirstName,
		Gender:       details.Gender,
		Email:        details.Email,
		LastName:     details.LastName,
		Phone:        details.Phone,
		IdType:       sql.NullString{String: details.IdType, Valid: len(details.IdType) > 0},
		IdNumber:     sql.NullString{String: details.IdNumber, Valid: len(details.IdNumber) > 0},
		UserName:     details.Email,
		PhotoID:      photoId,
		Organization: details.Organization,
		Activated:    true,
		Audience:     "staff",
	}

}

func (as *AccountService) replaceChangedFields(user *users.User, details *StaffMemberDetails, photoId string) {
	if len(details.FirstName) != 0 {
		user.FirstName = details.FirstName
	}
	if len(details.LastName) != 0 {
		user.LastName = details.LastName
	}
	if len(details.Gender) != 0 {
		user.Gender = details.Gender
	}
	if len(details.Email) != 0 {
		user.Email = details.Email
		user.UserName = details.Email
	}
	if len(details.Phone) != 0 {
		user.Phone = details.Phone
	}
	if len(details.IdType) != 0 || len(details.IdNumber) != 0 {
		user.IdType = sql.NullString{String: details.IdType, Valid: true}
		user.IdNumber = sql.NullString{String: details.IdNumber, Valid: true}
	}
	if len(photoId) != 0 {
		user.PhotoID = photoId
	}
	if len(details.Organization) != 0 {
		user.Organization = details.Organization
	}

}

func (as *AccountService) UpdateDetails(uid string, details *StaffMemberDetails, photoId string) error {
	user, err := as.userRepository.FetchByUID(uuid.FromStringOrNil(uid))
	if err != nil {
		return err
	}
	addedRoles, removedRoles := as.fetchRoleChanges(user, details.Roles)
	as.replaceChangedFields(user, details, photoId)
	return as.userRepository.Update(user, addedRoles, removedRoles)
}

func (as *AccountService) fetchRoleChanges(user *users.User, roles []string) ([]string, []string) {
	existingRoles := make(map[string]bool, 0)
	inputRoles := make(map[string]bool, 0)
	removedRoles := make([]string, 0)
	addedRoles := make([]string, 0)
	for _, role := range user.Roles {
		existingRoles[role.Name] = true
	}
	for _, entry := range roles {
		if existingRoles[entry] != true {
			addedRoles = append(addedRoles, entry)
		}
		inputRoles[entry] = true
	}
	for r, _ := range existingRoles {
		if inputRoles[r] != true {
			removedRoles = append(removedRoles, r)
		}
	}
	return addedRoles, removedRoles
}

func (as *AccountService) RequestPasswordReset(request *PasswordResetRequest) error {

	if request.HasValue() && request.IsByEmail() {
		return as.resetByEmail(request.Value)
	} else if request.HasValue() && request.IsByPhone() {
		return as.resetByPhone(request.Value)
	} else {
		return errors.New("PASSWORD RESET REQUEST IS INVALID")
	}
}

func (as *AccountService) resetByEmail(emailAddress string) error {
	user, err := as.userRepository.FetchUserByEmailAddress(emailAddress)
	if err != nil {
		return err
	}
	reference := uuid.NewV4().String()
	location, _ := time.LoadLocation("UTC")
	dueDate := time.Now().Add(24 * time.Hour).In(location)
	if err := as.recoveryRepository.Add(&recovery.RecoveryOrder{
		UID:       user.UID.String(),
		DueDate:   dueDate,
		Method:    recovery.PHONE_METHOD,
		Reference: reference,
	}); err != nil {
		return err
	}
	order := &messenger.PasswordResetOrder{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Reference: reference,
		Expiry:    time.Hour * 24,
	}
	return as.messengerClient.SendPasswordResetOrder(order)
}

func (as *AccountService) resetByPhone(phoneNumber string) error {
	return fmt.Errorf("This recovery method is not supported yet")
}

func (as *AccountService) ResetPassword(order *PasswordResetOrder) error {
	resetDetails, err := as.recoveryRepository.Fetch(order.Reference)
	if err != nil {
		return err
	}
	user, err := as.userRepository.FetchByUID(resetDetails.UID)
	if err != nil {
		return err
	}
	hash, err := as.passwordHandler.CreateHash(order.Password)
	if err != nil {
		return err
	}
	user.Password = hash
	return as.userRepository.Update(user, []string{}, []string{})
}

func (as *AccountService) DeRegister(uid string) error {
	return as.userRepository.Remove(uuid.FromStringOrNil(uid))
}

func (as *AccountService) Roles() ([]*roles.Role, error) {

	return as.rolesRepository.Fetch([]string{"SUPER_ADMINISTRATOR", "APPLICANT"})
}

func (as *AccountService) Search(Query string, Limit, Offset int64) ([]*users.User, error) {
	return as.userRepository.StaffSearch(Query, Limit, Offset)
}
