package staff

import (
	"errors"
	"mime/multipart"
	"net/http"
	"strings"
)

const (
	MAX_FORM_SIZE   = 5 * 1024 * 1024
	PROFILE_PICTURE = "photo"
)

type StaffMemberDetails struct {
	FirstName    string
	LastName     string
	Gender       string
	Email        string
	Phone        string
	IdType       string
	IdNumber     string
	Roles        []string
	Organization string
	About        string
}

type StaffMemberForm struct {
	FirstName    string
	LastName     string
	Email        string
	Gender       string
	Phone        string
	IdType       string
	IdNumber     string
	Photo        multipart.File
	PhotoHeader  *multipart.FileHeader
	Roles        []string
	Organization string
	About        string
}

func ParseForm(r *http.Request) (StaffMemberForm, error) {
	if err := r.ParseMultipartForm(MAX_FORM_SIZE); err != nil {
		return StaffMemberForm{}, err
	}
	file, header, _ := r.FormFile(PROFILE_PICTURE)

	return StaffMemberForm{
		FirstName:    r.FormValue("first_name"),
		LastName:     r.FormValue("last_name"),
		Gender:       r.FormValue("gender"),
		Email:        r.FormValue("email_address"),
		Phone:        r.FormValue("phone_number"),
		IdType:       r.FormValue("id_type"),
		IdNumber:     r.FormValue("id_number"),
		Photo:        file,
		PhotoHeader:  header,
		Roles:        r.Form["roles"],
		Organization: r.FormValue("organization"),
		About:        r.FormValue("about"),
	}, nil
}

func (smf *StaffMemberForm) HasPhoto() bool {
	if smf.PhotoHeader != nil && smf.PhotoHeader.Size > 0 {
		return true
	}
	return false
}

func (smf *StaffMemberForm) Validate() error {
	var sb strings.Builder
	if len(smf.FirstName) == 0 {
		sb.WriteString("First Name is Missing \n")
	}
	if len(smf.LastName) == 0 {
		sb.WriteString("Last Name is Missing \n")
	}
	if len(smf.Email) == 0 {
		sb.WriteString("Email Address is not given \n")
	}
	if len(smf.Phone) == 0 {
		sb.WriteString("Contact Phone Number has not been provided \n")
	}
	if len(smf.Gender) == 0 {
		smf.Gender = "UNKNOWN"
	}
	if len(smf.Organization) == 0 {
		sb.WriteString("Organization has not been given \n")
	}
	if len(smf.Roles) == 0 {
		sb.WriteString("No Roles have benn given \n")
	}
	result := sb.String()
	if len(result) != 0 {
		return errors.New(result)
	}
	return nil
}

func (smf *StaffMemberForm) ToDetails() *StaffMemberDetails {
	return &StaffMemberDetails{
		FirstName:    smf.FirstName,
		LastName:     smf.LastName,
		Gender:       smf.Gender,
		Email:        smf.Email,
		Phone:        smf.Phone,
		IdType:       smf.IdType,
		IdNumber:     smf.IdNumber,
		Roles:        smf.Roles,
		Organization: smf.Organization,
		About:        smf.About,
	}
}

type PasswordResetRequest struct {
	Mode  string `json:"mode"`
	Value string `json:"value"`
}

func (prr *PasswordResetRequest) IsByPhone() bool {
	return prr.Mode == "PHONE"
}

func (prr *PasswordResetRequest) IsByEmail() bool {
	return prr.Mode == "EMAIL"
}

func (prr *PasswordResetRequest) HasValue() bool {
	return len(prr.Value) != 0
}

type PasswordResetOrder struct {
	Reference string `json:"reference"`
	Password  string `json:"password"`
}
