package clients

import (
	"errors"
	"mime/multipart"
	"net/http"
	"strings"
)

const (
	MAX_FORM_SIZE   = 5 * 1024 * 1024
	PROFILE_PICTURE = "photo"
)

type OrdinaryUserDetails struct {
	FirstName string
	LastName  string
	Gender    string
	Email     string
	Phone     string
	IdType    string
	IdNumber  string
	Password  string
	Role      string
	About     string
}

type OrdinaryUserForm struct {
	FirstName   string
	LastName    string
	Email       string
	Gender      string
	Phone       string
	IdType      string
	IdNumber    string
	Photo       multipart.File
	PhotoHeader *multipart.FileHeader
	Role        string
	Password    string
	About       string
}

func ParseForm(r *http.Request) (*OrdinaryUserForm, error) {
	if err := r.ParseMultipartForm(MAX_FORM_SIZE); err != nil {
		return &OrdinaryUserForm{}, err
	}
	file, header, _ := r.FormFile(PROFILE_PICTURE)
	return &OrdinaryUserForm{
		FirstName:   r.FormValue("first_name"),
		LastName:    r.FormValue("last_name"),
		Gender:      r.FormValue("gender"),
		Email:       r.FormValue("email_address"),
		Phone:       r.FormValue("phone_number"),
		IdType:      r.FormValue("id_type"),
		IdNumber:    r.FormValue("id_number"),
		Password:    r.FormValue("password"),
		Photo:       file,
		PhotoHeader: header,
		Role:        "APPLICANT",
		About:       r.FormValue("about"),
	}, nil
}

func (ouf *OrdinaryUserForm) HasPhoto() bool {
	if ouf.PhotoHeader != nil && ouf.PhotoHeader.Size > 0 {
		return true
	}
	return false
}

func (ouf *OrdinaryUserForm) Validate() error {
	var sb strings.Builder
	if len(ouf.FirstName) == 0 {
		sb.WriteString("First Name is Missing \n")
	}
	if len(ouf.LastName) == 0 {
		sb.WriteString("Last Name is Missing \n")
	}
	if len(ouf.Gender) == 0 {
		sb.WriteString("Gender is not specified \n")
	}
	if len(ouf.Email) == 0 {
		sb.WriteString("Email Address is not given \n")
	}
	if len(ouf.Phone) == 0 {
		sb.WriteString("Contact Phone Number has not been provided \n")
	}
	if len(ouf.IdType) == 0 {
		sb.WriteString("Identification Type not specified \n")
	}
	if len(ouf.IdNumber) == 0 {
		sb.WriteString("Identification number not given \n")
	}
	if len(ouf.Password) == 0 {
		sb.WriteString("Password has not been given \n")
	}
	result := sb.String()
	if len(result) != 0 {
		return errors.New(result)
	}
	return nil
}

func (ouf *OrdinaryUserForm) ToDetails() *OrdinaryUserDetails {
	return &OrdinaryUserDetails{
		FirstName: ouf.FirstName,
		LastName:  ouf.LastName,
		Gender:    ouf.Gender,
		Email:     ouf.Email,
		Phone:     ouf.Phone,
		IdType:    ouf.IdType,
		IdNumber:  ouf.IdNumber,
		Password:  ouf.Password,
		Role:      ouf.Role,
		About:     ouf.About,
	}
}

type ActivationDetails struct {
	Reference string `json:"reference"`
	Value     string `json:"value"`
}

type OTPDetails struct {
	value string
}

type PasswordResetRequest struct {
	Mode  string `json:"channel"`
	Value string `json:"channel_value"`
}

func (prr *PasswordResetRequest) IsByPhone() bool {
	return prr.Mode == "PHONE"
}

func (prr *PasswordResetRequest) IsByEmail() bool {
	return prr.Mode == "EMAIL"
}

func (prr *PasswordResetRequest) HasValue() bool {
	return len(prr.Value) != 0
}

type PasswordResetOrder struct {
	Reference string `json:"reference"`
	Password  string `json:"password"`
}
