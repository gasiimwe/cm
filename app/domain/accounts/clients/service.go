package clients

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/messenger"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/otp"
	"gitlab.com/gasiimwe/cm/app/domain/accounts/recovery"
	"gitlab.com/gasiimwe/cm/app/domain/acm/users"
	"gitlab.com/gasiimwe/cm/app/infrastructure/logging"
	"gitlab.com/gasiimwe/cm/app/infrastructure/security"
)

type AccountService struct {
	userRepository     users.UserRepository
	recoveryRepository recovery.RecoveryRepository
	passwordHandler    security.PasswordHandler
	otpHandler         *otp.OTPHandler
	messengerClient    messenger.Client
}

func New(
	UserRepository users.UserRepository,
	RecoveryRepository recovery.RecoveryRepository,
	PasswordHandler security.PasswordHandler,
	OtpHandler *otp.OTPHandler,
	MessengerClient messenger.Client) *AccountService {
	return &AccountService{
		userRepository:     UserRepository,
		recoveryRepository: RecoveryRepository,
		passwordHandler:    PasswordHandler,
		otpHandler:         OtpHandler,
		messengerClient:    MessengerClient,
	}
}

func (as *AccountService) Register(details *OrdinaryUserDetails, photoId string) error {
	user := as.createUser(details, photoId)
	hash, err := as.passwordHandler.CreateHash(details.Password)
	if err != nil {
		return err
	}
	user.Password = hash
	if err := as.userRepository.Add(user, []string{details.Role}); err != nil {
		return err
	}
	reference, otp, err := as.otpHandler.Create(user.UID.String())
	if err != nil {
		return err
	}
	order := &messenger.OTPConfirmationOrder{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		OTPValue:  otp,
		Reference: reference,
		Expiry:    as.otpHandler.GetDuration()}
	return as.messengerClient.SendOTPOrder(order)
}

func (as *AccountService) createUser(details *OrdinaryUserDetails, photoId string) *users.User {

	return &users.User{
		UID:       uuid.NewV4(),
		FirstName: details.FirstName,
		Gender:    details.Gender,
		Email:     details.Email,
		LastName:  details.LastName,
		Phone:     details.Phone,
		IdType:    sql.NullString{String: details.IdType, Valid: len(details.IdType) > 0},
		IdNumber:  sql.NullString{String: details.IdNumber, Valid: len(details.IdNumber) > 0},
		UserName:  details.Email,
		Password:  details.Password,
		PhotoID:   photoId,
		Activated: false,
		Audience:  "applicant",
		About:     details.About,
	}
}

func (as *AccountService) replaceChangedFields(user *users.User, details *OrdinaryUserDetails, photoId string) {
	if len(details.FirstName) != 0 {
		user.FirstName = details.FirstName
	}
	if len(details.LastName) != 0 {
		user.LastName = details.LastName
	}
	if len(details.Gender) != 0 {
		user.Gender = details.Gender
	}
	if len(details.Email) != 0 {
		user.Email = details.Email
		user.UserName = details.Email
	}
	if len(details.Phone) != 0 {
		user.Phone = details.Phone
	}
	if len(details.IdType) != 0 || len(details.IdNumber) != 0 {
		user.IdType = sql.NullString{String: details.IdType, Valid: true}
		user.IdNumber = sql.NullString{String: details.IdNumber, Valid: true}
	}
	if len(photoId) != 0 {
		user.PhotoID = photoId
	}

	if len(details.About) != 0 {
		user.About = details.About
	}
}

func (as *AccountService) ActivateOverOTP(reference string, value string) error {
	LOG := logging.GetInstance()
	if len(reference) == 0 || len(value) == 0 {
		return fmt.Errorf("This is invalid REFERENCE:%s VALUE:%s", reference, value)
	}
	LOG.Infof("RECEIVED REFERENCE: %s WITH VALUE: %s", reference, value)
	uid, err := as.otpHandler.FetchMatch(reference, value)
	if err != nil {
		return err
	}
	user, err := as.userRepository.FetchByUID(uuid.FromStringOrNil(uid))
	if err != nil {
		return err
	}
	user.Activated = true
	LOG.Infof("ACTIVATING USER")
	return as.userRepository.Update(user, []string{}, []string{})
}

func (as *AccountService) UpdateDetails(uid string, details *OrdinaryUserDetails, photoId string) error {
	user, err := as.userRepository.FetchByUID(uuid.FromStringOrNil(uid))
	if err != nil {
		return err
	}
	as.replaceChangedFields(user, details, photoId)
	return as.userRepository.Update(user, []string{}, []string{})
}

func (as *AccountService) RequestPasswordReset(request *PasswordResetRequest) error {

	if request.HasValue() && request.IsByEmail() {
		return as.resetByEmail(request.Value)
	} else if request.HasValue() && request.IsByPhone() {
		return as.resetByPhone(request.Value)
	} else {
		return errors.New("PASSWORD RESET REQUEST IS INVALID")
	}
}

func (as *AccountService) resetByEmail(emailAddress string) error {
	user, err := as.userRepository.FetchUserByEmailAddress(emailAddress)
	if err != nil {
		return err
	}
	reference := uuid.NewV4().String()
	if err := as.recoveryRepository.Add(&recovery.RecoveryOrder{
		UID:       user.UID.String(),
		DueDate:   time.Now().Add(24 * time.Hour),
		Method:    recovery.PHONE_METHOD,
		Reference: reference,
	}); err != nil {
		return err
	}
	order := &messenger.PasswordResetOrder{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Reference: reference,
		Expiry:    24 * time.Hour}
	return as.messengerClient.SendPasswordResetOrder(order)
}

func (as *AccountService) resetByPhone(phoneNumber string) error {
	return fmt.Errorf("We Currently do not support this recovery option")
}

func (as *AccountService) ResetPassword(order *PasswordResetOrder) error {
	resetDetails, err := as.recoveryRepository.Fetch(order.Reference)
	if err != nil {
		return err
	}
	user, err := as.userRepository.FetchByUID(resetDetails.UID)
	if err != nil {
		return err
	}
	hash, err := as.passwordHandler.CreateHash(order.Password)
	if err != nil {
		return err
	}
	user.Password = hash
	return as.userRepository.Update(user, []string{}, []string{})
}

func (as *AccountService) DeRegister(uid string) error {
	return as.userRepository.Remove(uuid.FromStringOrNil(uid))
}
