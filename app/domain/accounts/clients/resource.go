package clients

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gasiimwe/cm/app/domain/accounts/storage"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/controllers/web"
	"gitlab.com/gasiimwe/cm/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/account-service/accounts/clients"

type ClientController struct {
	*web.ControllerBase
	accountService *AccountService
	fileStorage    storage.FileStorage
}

func NewController(accountService *AccountService, fileStorage storage.FileStorage) web.HTTPController {
	return &ClientController{
		accountService: accountService,
		fileStorage:    fileStorage,
	}
}

func (cc *ClientController) Name() string {
	return "clients-controller"
}

func (cc *ClientController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.Add(
		BASE_PATH,
		false,
		"POST",
		cc.register,
	)
	RouteRegistry.Add(
		BASE_PATH+"/activate",
		false,
		"PUT",
		cc.activate,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"APPLICANT"},
		"PUT",
		cc.update,
	)
	RouteRegistry.Add(
		BASE_PATH+"/images/{image-identifier}",
		false,
		"GET",
		cc.photo,
	)
	RouteRegistry.AddRestricted(
		BASE_PATH,
		[]string{"APPLICANT"},
		"DELETE",
		cc.remove,
	)
	RouteRegistry.Add(
		BASE_PATH+"/recover-password",
		false,
		"PUT",
		cc.recoverPassword,
	)
	RouteRegistry.Add(
		BASE_PATH+"/reset-password",
		false,
		"PUT",
		cc.resetPassword,
	)
}

func (cc *ClientController) register(w http.ResponseWriter, r *http.Request) {
	var photoReference string
	form, err := ParseForm(r)
	if err != nil {
		cc.BadRequest(w, err)
		return
	} else if err := form.Validate(); err != nil {
		cc.BadRequest(w, err)
		return
	} else if form.HasPhoto() {
		defer form.Photo.Close()
		if photoReference, err = cc.fileStorage.Save(form.PhotoHeader.Filename, form.Photo); err != nil {
			cc.ServiceFailure(w, err)
			return
		}
	}
	details := form.ToDetails()
	if err := cc.accountService.Register(
		details,
		photoReference,
	); err != nil {
		cc.ServiceFailure(w, err)
	} else {
		cc.OKNoResponse(w)
	}
}

func (cc *ClientController) activate(w http.ResponseWriter, r *http.Request) {
	activationDetails := &ActivationDetails{}
	if err := json.NewDecoder(r.Body).Decode(activationDetails); err != nil {
		cc.BadRequest(w, err)
	} else {
		if err := cc.accountService.ActivateOverOTP(activationDetails.Reference, activationDetails.Value); err != nil {
			cc.ServiceFailure(w, err)
		} else {
			cc.OKNoResponse(w)
		}
	}
}

func (cc *ClientController) update(w http.ResponseWriter, r *http.Request) {
	var photoReference string
	form, err := ParseForm(r)
	if err != nil {
		cc.BadRequest(w, err)
	} else if form.HasPhoto() {
		defer form.Photo.Close()
		if photoReference, err = cc.fileStorage.Save(form.PhotoHeader.Filename, form.Photo); err != nil {
			cc.ServiceFailure(w, err)
		}
	}
	details := form.ToDetails()
	uid := cc.GetPrincipals(r).ID
	if err := cc.accountService.UpdateDetails(uid, details, photoReference); err != nil {
		cc.ServiceFailure(w, err)
	} else {
		cc.OKNoResponse(w)
	}
}

func (cc *ClientController) photo(w http.ResponseWriter, r *http.Request) {
	imageIdentifier := cc.GetVar("image-identifier", r)
	cc.fileStorage.Fetch(imageIdentifier, w)
}

func (cc *ClientController) remove(w http.ResponseWriter, r *http.Request) {
	uid := cc.GetPrincipals(r).ID
	if err := cc.accountService.DeRegister(uid); err != nil {
		cc.ServiceFailure(w, err)
	} else {
		cc.OKNoResponse(w)
	}
}

func (cc *ClientController) recoverPassword(w http.ResponseWriter, r *http.Request) {
	resetRequest := &PasswordResetRequest{}
	if err := json.NewDecoder(r.Body).Decode(resetRequest); err != nil {
		cc.BadRequest(w, err)
	} else if err = cc.accountService.RequestPasswordReset(resetRequest); err != nil {
		cc.ServiceFailure(w, err)
	} else {
		cc.OKNoResponse(w)
	}
}

func (cc *ClientController) resetPassword(w http.ResponseWriter, r *http.Request) {
	resetOrder := &PasswordResetOrder{}
	if err := json.NewDecoder(r.Body).Decode(resetOrder); err != nil {
		cc.BadRequest(w, err)
	} else if err = cc.accountService.ResetPassword(resetOrder); err != nil {
		cc.ServiceFailure(w, err)
	} else {
		cc.OKNoResponse(w)
	}
}
