#!/usr/bin/env bash

go-bindata -o ./app/infrastructure/server/resources.go ./resources/ ./resources/migrations
sed 's/package main/package server/g' ./app/infrastructure/server/resources.go > ./app/infrastructure/server/content.go
rm ./app/infrastructure/server/resources.go
mv ./app/infrastructure/server/content.go ./app/infrastructure/server/resources.go