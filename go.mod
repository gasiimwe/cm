module gitlab.com/gasiimwe/cm

go 1.15

require (
	github.com/armon/go-metrics v0.3.5 // indirect
	github.com/aws/aws-sdk-go v1.27.0
	github.com/fatih/color v1.10.0 // indirect
	github.com/gdamore/tcell/v2 v2.1.0 // indirect
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/gojektech/heimdall/v6 v6.1.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/golang/protobuf v1.4.3
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/consul/api v1.8.1
	github.com/hashicorp/go-hclog v0.15.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jasonlvhit/gocron v0.0.1
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.4
	github.com/prometheus/client_golang v1.8.0
	github.com/rivo/tview v0.0.0-20210125085121-dbc1f32bb1d0
	github.com/satori/go.uuid v1.2.0
	github.com/sethvargo/go-password v0.2.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	google.golang.org/protobuf v1.25.0
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v2 v2.3.0
	gorm.io/driver/postgres v1.0.6
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.8
)
